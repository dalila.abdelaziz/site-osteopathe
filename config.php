<?php 
// On définit des constantes de configuration.
// En POO, on nomme les constantes en MAJ.
define('SITE_ROOT', dirname(__FILE__)); // Renvoie C:\xampp\htdocs\projets\osteo-mvc
define('BASE_FOLDER', "/projets/osteo-mvc");
// Déclaration de l'URL d'accès à la racine du site (possibilité donc de l'utiliser dans lors de l'appel de notre css).
define('SCRIPT_ROOT', 'http://localhost/projets/osteo-mvc'); 
// On définit si le ConnexionController fonctionne avec le jeu de données en connexion locale ou distante (en l'occurrence local pour le moment).
define('IS_ONLINE', false);