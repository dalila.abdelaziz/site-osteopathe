<?php

namespace App\Controller;

use App\Model\Home;

class HomeController{
    // Propriétés
    // On crée une variable qui servira à instancier un model de la class Home
    private $home_bandeau;
    private $home_presentation;
    private $home_contact;
    private $home_horaire;
    private $home_publik;
    private $home_patho;

    // Constructeur
    public function __construct()
    {
        $this->home_bandeau = new Home();
        $this->home_presentation = new Home();
        $this->home_contact = new Home();
        $this->home_horaire = new Home();
        $this->home_publik = new Home();
        $this->home_patho = new Home();
    }

    // Méthodes
    // Fonction qui retourne les données de la table à la view
    public function list(){
        // On demande la liste du contenu de la page home au modèle(Home.php)
        $home_bandeaux = $this->home_bandeau->getBandeauHome();
        $home_presentations= $this->home_presentation->getPresentation();
        $home_contacts = $this->home_contact->getContactHome();
        $home_horaires = $this->home_horaire->getHoraireHome();
        $home_publiks = $this->home_publik->getPublik();
        $home_pathos = $this->home_patho->getPatho();
        // On affiche la vue qui va pouvoir travailler avec les variables
        require_once('view/admin-home.php');
    }

    // Fonctions qui permettent l'ajout en base de données
    public function addBandeauHome($post){
        $this->home_bandeau->addBandeauHome($post);
        header('Location:'.BASE_FOLDER.'/admin/accueil');
    }
    public function addPresentation($post, $files){
        $this->home_presentation->addPresentation($post, $files);
        header('Location:'.BASE_FOLDER.'/admin/accueil');
    }
    public function addContactHome($post){
        $this->home_contact->addContactHome($post);
        header('Location:'.BASE_FOLDER.'/admin/accueil');
    }
    public function addHoraireHome($post){
        $this->home_horaire->addHoraireHome($post);
        header('Location:'.BASE_FOLDER.'/admin/accueil');
    }
    public function addPublik($post, $files){
        $this->home_publik->addPublik($post, $files);
        header('Location:'.BASE_FOLDER.'/admin/accueil');
    }
    public function addPatho($post, $files){
        $this->home_patho->addPatho($post, $files);
        header('Location:'.BASE_FOLDER.'/admin/accueil');
    }

    // Fonctions qui permettent de récupérer par l'id pour éditer un objet
    public function editBandeauHome($id){
        // On appelle une méthode du modèle qui récupère un article par son id
        $home_bandeau = $this->home_bandeau->getByIdBandeauHome($id);
        // On affiche la vue correspondant à l'action
        require_once('view/edit-bandeau-home.php');
    }
    public function editPresentation($id){
        // On appelle une méthode du modèle qui récupère un article par son id
        $home_presentation = $this->home_presentation->getByIdPresentation($id);
        // On affiche la vue correspondant à l'action
        require_once('view/edit-presentation.php');
    }
    public function editContactHome($id){
        // On appelle une méthode du modèle qui récupère un article par son id
        $home_contact = $this->home_contact->getByIdContactHome($id);
        // On affiche la vue correspondant à l'action
        require_once('view/edit-contact-home.php');
    }
    public function editHoraireHome($id){
        // On appelle une méthode du modèle qui récupère un article par son id
        $home_horaire = $this->home_horaire->getByIdHoraireHome($id);
        // On affiche la vue correspondant à l'action
        require_once('view/edit-horaire-home.php');
    }
    public function editPublik($id){
        // On appelle une méthode du modèle qui récupère un article par son id
        $home_publik = $this->home_publik->getByIdPublik($id);
        // On affiche la vue correspondant à l'action
        require_once('view/edit-publik.php');
    }
    public function editPatho($id){
        // On appelle une méthode du modèle qui récupère un article par son id
        $home_patho = $this->home_patho->getByIdPatho($id);
        // On affiche la vue correspondant à l'action
        require_once('view/edit-patho.php');
    }

    // Fonctions qui permettent d'envoyer le formulaire d'update
    public function updateBandeauHome($post){
        $this->home_bandeau->updateBandeauHome($post);
        header('Location:'.BASE_FOLDER.'/admin/accueil');
    }
    public function updatePresentation($post, $files){
        $this->home_presentation->updatePresentation($post, $files);
        header('Location:'.BASE_FOLDER.'/admin/accueil');
    }
    public function updateContactHome($post){
        $this->home_contact->updateContactHome($post);
        header('Location:'.BASE_FOLDER.'/admin/accueil');
    }
    public function updateHoraireHome($post){
        $this->home_horaire->updateHoraireHome($post);
        header('Location:'.BASE_FOLDER.'/admin/accueil');
    }
    public function updatePublik($post, $files){
        $this->home_publik->updatePublik($post, $files);
        header('Location:'.BASE_FOLDER.'/admin/accueil');
    }
    public function updatePatho($post, $files){
        $this->home_patho->updatePatho($post, $files);
        header('Location:'.BASE_FOLDER.'/admin/accueil');
    }

    // Fonctions qui permettent de supprimer les données
    public function deleteBandeauHome($id){
        $this->home_bandeau->deleteBandeauHome($id);
        header('Location:'.BASE_FOLDER.'/admin/accueil');
    }
    public function deletePresentation($id){
        $this->home_presentation->deletePresentation($id);
        header('Location:'.BASE_FOLDER.'/admin/accueil');
    }
    public function deleteContactHome($id){
        $this->home_contact->deleteContactHome($id);
        header('Location:'.BASE_FOLDER.'/admin/accueil');
    }
    public function deleteHoraireHome($id){
        $this->home_horaire->deleteHoraireHome($id);
        header('Location:'.BASE_FOLDER.'/admin/accueil');
    }
    public function deletePublik($id){
        $this->home_publik->deletePublik($id);
        header('Location:'.BASE_FOLDER.'/admin/accueil');
    }
    public function deletePatho($id){
        $this->home_patho->deletePatho($id);
        header('Location:'.BASE_FOLDER.'/admin/accueil');
    }
}