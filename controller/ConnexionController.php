<?php

namespace OsteoMvc\Controller;

use PDO;

class ConnexionController
{
    // PROPRIETES
    private $host;
    private $user;
    private $password;
    private $bdd;
    public $connexion;
    // CONSTRUCTEUR
    public function __construct()
    {
        // On détermine ici le jeu de variables de connexion à utiliser d'après la configuration : IS_ONLINE. 
        if (!IS_ONLINE) {
            $this->host = "localhost";
            $this->user = "root";
            $this->password = "";
            $this->bdd = "osteo_mvc";
        } else {
            $this->host = "";
            $this->user = "";
            $this->password = "";
            $this->bdd = "";
        }
        // Connexion au serveur en PDO (PHP Data Object)
        // Si la variable connexion n'est pas définie.
        if (!isset($this->connexion)) {
            // Alors on essaie de se connecter avec un try.
            try {
                $this->connexion = new PDO("mysql:host=$this->host;dbname=$this->bdd", $this->user, $this->password);
                // Sinon, le catch déclenche une erreur.
            } catch (\PDOException $e) {
                throw new \PDOException($e->getMessage(), (int)$e->getCode());
            }
        }
        // Enfin, on renvoie la connexion.
        return $this->connexion;
    }
    // METHODES

    // GETTERS / SETTERS

}
