<?php

namespace App\Controller;

use App\Model\Conseil;

class ConseilController{
    // Propriétés
    // On crée une variable qui servira à instancier un model de la class conseil
    private $conseilBandeau;
    private $conseilArticle;

    // Constructeur
    public function __construct()
    {
        $this->conseilBandeau = new Conseil();
        $this->conseilArticle = new Conseil();
    }

    // Méthodes
    public function show(){
        // On demande la liste du contenu de la page conseil au modèle(Conseil.php)
        $conseilArticles = $this->conseilArticle->getArticle();
        $conseilBandeaux = $this->conseilBandeau->getBandeau();
        // On affiche la vue qui va pouvoir travailler avec la variable $conseils
        require_once("view/conseils.php");
    }
    public function list(){
        // On demande la liste du contenu de la page conseil au modèle(Conseil.php)
        $conseilArticles = $this->conseilArticle->getArticle();
        $conseilBandeaux = $this->conseilBandeau->getBandeau();
        // On affiche la vue qui va pouvoir travailler avec la variable $conseils
        require_once("view/admin-conseils.php");
    }

    /**
     * Fonction qui prend charge l'ajout en base de données d'un article
     *
     * @return void
     */
    public function addArticle($post, $files){
        $this->conseilArticle->addArticle($post, $files);
        header('Location:'.BASE_FOLDER.'/admin/conseils');
    }
    public function addBandeau($post){
        $this->conseilBandeau->addBandeau($post);
        header('Location:'.BASE_FOLDER.'/admin/conseils');
    }
    public function editArticle($id){
        // On appelle une méthode du modèle qui récupère un article par son id
        $conseilArticle = $this->conseilArticle->getByIdArticle($id);
        // On affiche la vue correspondant à l'action
        require_once('view/edit-conseil-article.php');
    }
    public function editBandeau($id){
        // On appelle une méthode du modèle qui récupère un article par son id
        $conseilBandeau = $this->conseilBandeau->getByIdBandeau($id);
        // On affiche la vue correspondant à l'action
        require_once('view/edit-conseil-bandeau.php');
    }
    public function updateArticle($post, $files){
        $this->conseilArticle->updateArticle($post, $files);
        header('Location:'.BASE_FOLDER.'/admin/conseils');
    }
    public function updateBandeau($post){
        $this->conseilBandeau->updateBandeau($post);
        header('Location:'.BASE_FOLDER.'/admin/conseils');
    }
    public function deleteBandeau($id){
        $this->conseilBandeau->deleteBandeau($id);
        header('Location:'.BASE_FOLDER.'/admin/conseils');
    }
    public function deleteArticle($id){
        $this->conseilArticle->deleteArticle($id);
        header('Location:'.BASE_FOLDER.'/admin/conseils');
    }
}