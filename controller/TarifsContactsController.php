<?php
// On met en place un namespace qui représente une manière abstraite de ranger les fichiers. Càd que l'on crée des dossiers virtuels auxquels sont associés les fichiers.
// Les namespaces permettent ensuite l'utilisation de use.
namespace OsteoMvc\Controller;

use OsteoMvc\Model\TarifsContacts;

class TarifsContactsController
{
    // PROPRIETES
    // On crée une variable qui servira à instancier un modèle de la classe TarifsContacts.
    private $tarifsContacts;
    private $tarifscontactsBandeau;
    private $tarifscontactsTarif;
    private $tarifscontactsContact;
    private $tarifscontactsInfoComp;
    // CONTRUCTEUR
    public function __construct()
    {
        $this->tarifsContacts = new TarifsContacts();
        $this->tarifscontactsBandeau = new TarifsContacts();
        $this->tarifscontactsTarif = new TarifsContacts();
        $this->tarifscontactsContact = new TarifsContacts();
        $this->tarifscontactsInfoComp = new TarifsContacts();
    }

    // METHODES

    public function listAdmin()
    {
        $tarifscontactsBandeaux = $this->tarifscontactsBandeau->getAllBandeau();
        $tarifscontactsTarifs = $this->tarifscontactsTarif->getAllTarif();
        $tarifscontactsContacts = $this->tarifscontactsContact->getAllContact();
        $tarifscontactsInfosComps = $this->tarifscontactsInfoComp->getAllInfoComp();
        require_once('view/tarifs-contacts.php');
    }

    // Gestion des BANDEAUX

    /**
     * Fonction prenant en charge l'ajout de nouveaux contenus du bandeau en BDD.
     *
     * @return void
     */
    public function addBandeau($post)
    {
        $this->tarifscontactsBandeau->addBandeau($post);
        header('Location:' . BASE_FOLDER . '/admin/tarifs-contacts');
    }

    /**
     * Fonction prenant en charge la possibilité de modifier les contenus d'un bandeau.
     *
     * @param [type] $id
     * @return void
     */
    public function editBandeau($id)
    {
        // On appelle une méthode du modèle qui récupère un bandeau par son id. 
        $tarifscontactsBandeau = $this->tarifscontactsBandeau->getByIdBandeau($id);
        // On affiche la vue associée à l'objectif (ici modifier un bandeau).
        require_once('view/tc-edit-bandeau.php');
    }

    /**
     * Fonction prenant en charge le post des contenus d'un bandeau après modification.
     *
     * @param [type] $post
     * @return void
     */
    public function updateBandeau($post)
    {
        $this->tarifscontactsBandeau->updateBandeau($post);
        header('Location:' . BASE_FOLDER . '/admin/tarifs-contacts');
    }

    /**
     * Fonction prenant en charge la suppression d'un bandeau et son contenu. 
     *
     * @param [type] $id
     * @return void
     */
    public function deleteBandeau($id)
    {
        $this->tarifscontactsBandeau->deleteBandeau($id);
    }

    // Gestion des TARIFS & PRESTA

    /**
     * Fonction prenant en charge l'ajout d'un nouveau duo presta/tarif en BDD.
     *
     * @return void
     */
    public function addTarif($post)
    {
        $this->tarifscontactsTarif->addTarif($post);
        header('Location:' . BASE_FOLDER . '/admin/tarifs-contacts');
    }

    /**
     * Fonction prenant en charge la possibilité de modifier les contenus d'un duo presta/tarif.
     *
     * @param [type] $id
     * @return void
     */
    public function editTarif($id)
    {
        // On appelle une méthode du modèle qui récupère un duo presta/tarif par son id. 
        $tarifscontactsTarif = $this->tarifscontactsTarif->getByIdTarif($id);
        // On affiche la vue associée à l'objectif (ici modifier un duo presta/tarif).
        require_once('view/tc-edit-tarif.php');
    }

    /**
     * Fonction prenant en charge le post des nouveaux contenus d'un duo presta/tarif pré-existant après modification.
     *
     * @param [type] $post
     * @return void
     */
    public function updateTarif($post)
    {
        $this->tarifscontactsTarif->updateTarif($post);
        header('Location:' . BASE_FOLDER . '/admin/tarifs-contacts');
    }

    /**
     * Fonction prenant en charge la suppression d'un duo presta/tarif et son contenu. 
     *
     * @param [type] $id
     * @return void
     */
    public function deleteTarif($id)
    {
        $this->tarifscontactsTarif->deleteTarif($id);
    }

    // Gestion des CONTACTS

    /**
     * Fonction prenant en charge l'ajout d'un nouveau contact en BDD.
     *
     * @param [type] $post
     * @return void
     */
    public function addContact($post)
    {
        $this->tarifscontactsContact->addContact($post);
        header('Location:' . BASE_FOLDER . '/admin/tarifs-contacts');
    }

    /**
     * Fonction prenant en charge la possibilité de modifier les contenus d'un contact pré-existant. 
     *
     * @param [type] $id
     * @return void
     */
    public function editContact($id)
    {
        $tarifscontactsContact = $this->tarifscontactsContact->getByIdContact($id);
        require_once('view/tc-edit-contact.php');
    }

    /**
     * Fonction prenant en charge le post des nouveaux contenus d'un contact pré-existant après modification.
     *
     * @param [type] $post
     * @return void
     */
    public function updateContact($post)
    {
        $this->tarifscontactsContact->updateContact($post);
        header('Location:' . BASE_FOLDER . '/admin/tarifs-contacts');
    }

    /**
     * Fonction prenant en charge la suppression d'un contact ainsi que ses contenus de la BDD.
     *
     * @param [type] $id
     * @return void
     */
    public function deleteContact($id)
    {
        $this->tarifscontactsContact->deleteContact($id);
    }

    // Gestion des INFOS COMPLEMENTAIRES

    /**
     * Fonction prenant en charge l'ajout d'un nouveau groupe d'infos complémentaires en BDD.
     *
     * @param [type] $post
     * @return void
     */
    public function addInfoComp($post)
    {
        $this->tarifscontactsInfoComp->addInfoComp($post);
        header('Location:'.BASE_FOLDER.'/admin/tarifs-contacts');
    }

    /**
     * Fonction prenant en charge la possibilité de modifier les contenus d'un groupe d'infos complémentaires pré-existant.
     *
     * @param [type] $id
     * @return void
     */
    public function editInfoComp($id)
    {
        $tarifscontactsInfoComp = $this->tarifscontactsInfoComp->getByIdInfoComp($id);
        require_once('view/tc-edit-infocomp.php');
    }

    /**
     * Fonction prenant en charge la publication des nouveaux contenus d'un groupe d'infos complémentaires pré-existant après modification.
     *
     * @param [type] $post
     * @return void
     */
    public function updateInfoComp($post)
    {
        $this->tarifscontactsInfoComp->updateInfoComp($post);
        header('Location:'.BASE_FOLDER.'/admin/tarifs-contacts');
    }

    /**
     * Fonction prenant en charge la suppression d'un groupe d'infos complémentaires ainsi que ses contenus de la BDD.
     *
     * @param [type] $id
     * @return void
     */
    public function deleteInfoComp($id)
    {
        $this->tarifscontactsInfoComp->deleteInfoComp($id);
    }

    // /**
    //  * Fonction prenant en charge l'ajout de nouveaux tarifs en BDD.
    //  *
    //  * @return void
    //  */
    // public function addTarif(){
    //     $this->tarifscontactsTarif->addTarif($post);
    //     // On redirige ici vers la vue dédiée à l'action d'ajout.
    //     header('Location:'.BASE_FOLDER.'/admin/tarifs-contacts');
    // }

    // /**
    //  * Fonction prenant en charge l'ajout de nouveaux contacts en BDD.
    //  *
    //  * @return void
    //  */
    // public function addContact(){
    //     $this->tarifscontactsContact->addContact($post);
    //     // On redirige ici vers la vue dédiée à l'action d'ajout.
    //     header('Location:'.BASE_FOLDER.'/admin/tarifs-contacts');
    // }

    // /**
    //  * Fonction prenant en charge l'ajout de nouvelles infos complémentaires en BDD.
    //  *
    //  * @return void
    //  */
    // public function addInfoComp(){
    //     $this->tarifscontactsInfoComp->addInfoComp($post);
    //     header('Location:'.BASE_FOLDER.'/admin/tarifs-contacts');
    // }

    // GETTERS / SETTERS

}


// CODE DALILA

// public function list(){
//     // On demande la liste du contenu de la page conseil au modèle(Conseil.php)
//     $conseilArticles = $this->conseilArticle->getArticle();
//     $conseils = $this->conseil->getBandeau();
//     // On affiche la vue qui va pouvoir travailler avec la variable $conseils
//     require_once("view/admin-conseils.php");
// }

// /**
//  * Fonction qui prend charge l'ajout en base de données d'un article
//  *
//  * @return void
//  */
// public function addArticle($post, $files){
//     $this->conseilArticle->addArticle($post, $files);
//     header('Location:'.BASE_FOLDER.'/admin/conseils');
// }
// public function addBandeau($post){
//     $this->conseil->addBandeau($post);
//     header('Location:'.BASE_FOLDER.'/admin/conseils');
// }
