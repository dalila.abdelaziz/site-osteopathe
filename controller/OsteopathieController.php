<?php
// On met en place un namespace qui représente une manière abstraite de ranger les fichiers. Càd que l'on crée des dossiers virtuels auxquels sont associés les fichiers.
// Les namespaces permettent ensuite l'utilisation de use.
namespace OsteoMvc\Controller;

use OsteoMvc\Model\Osteopathie;

class OsteopathieController
{
    // PROPRIETES
    // On crée une variable qui servira à instancier un modèle de la classe Osteopathie.
    private $osteopathie;
    private $osteopathieIndication;

    // CONSTRUCTEUR
    public function __construct()
    {
        $this->osteopathie = new Osteopathie();
        $this->osteopathieIndication = new Osteopathie();
    }

    // METHODES
    public function listAdmin()
    {
        $osteopathieIndications = $this->osteopathieIndication->getAllIndication();
        require_once('view/osteopathie.php');
    }

    // Gestion des INDICATIONS
    public function addIndication($post, $files)
    {
        $this->osteopathieIndication->addIndication($post, $files);
        header('Location:' . BASE_FOLDER . '/admin/osteopathie');
    }

    public function editIndication($id)
    {
        $osteopathieIndication = $this->osteopathieIndication->getByIdIndication($id);
        require_once('view/o-edit-indication.php');
    }

    public function updateIndication($post, $files)
    {
        $this->osteopathieIndication->updateIndication($post, $files);
        header('Location:' . BASE_FOLDER . '/admin/osteopathie');
    }

    public function deleteIndication($id)
    {
        $this->osteopathieIndication->deleteIndication($id);
    }

    // GETTERS / SETTERS

}
