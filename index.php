<?php
require_once('./config.php');
// On appelle ici le code du routeur via un require_once (afin d'aller chercher toutes les routes dans le fichier Router.php).
require_once('./core/Router.php');
// On instancie un routeur et on lance sa méthode publique execute(); 
$router = new Router();
$router->execute();