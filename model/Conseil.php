<?php

namespace App\Model;

use PDO;
use App\Controller\ConnexionController;

class Conseil{
    // Propriétés
    // On déclare des propriétés privées correspondant aux colonnes de la table
    private $titre;
    private $soustitre;
    private $paratexte;
    private $titre_article;
    private $image;
    // Je déclare une variable pour le fichier physique de l'image
    private $imageFile;
    private $texte;
    private $createdAt;
    // On déclare une variable qui recevra une instance de ConnexionController pour les transactions avec la BDD
    private $cc;

    // Constructeur
    public function __construct()
    {
        // On instancie un ConnexionController
        $this->cc = new ConnexionController();
    }

    // Méthodes
    // Fonction add pour envoyer les formulaires d'ajout
    /**
     * Fonction qui permet d'ajouter du contenu pour la page conseil
     *
     * @param [type] $post
     * @param [type] $files
     * @return void
     */
    public function addArticle($post, $files){
        // 1. Traitement du fichier image
        $fileName = "";
        if(isset($files['image']['tmp_name'])){
            // On renomme l'image
            // 1. On récupère l'extension du fichier image à l'aide de pathinfo
            $ext = pathinfo($files['image']['name'], PATHINFO_EXTENSION);
            // 2. On crée un nom unique pour l'image
            $fileName = "conseil_".uniqid().'.'.$ext;
            // 3. On détermine la destination du fichier
            $destination = "assets/images/";
            // 4. On déplace l'image (le fichier temporaire sur le serveur pour le mettre dans le dossier des assets et le renommer à la volée)
            move_uploaded_file($files['image']['tmp_name'], $destination.$fileName);
        }

        // 2. On traite les données postées
        $titre_article = addslashes(trim(ucfirst($post['titre_article'])));
        $texte = addslashes(trim($post['texte']));
        $createdAt = $post['createdAt'];

        // 3. On fait la requête d'ajout en PDO
        $req = $this->cc->connexion->prepare('INSERT INTO conseils_article (titre_article, texte, image, createdAt) VALUES (:titre_article, :texte, :image, :createdAt)');

        // 4. On prépare le tableau de données
        $datas = array(
            ':titre_article' => $titre_article,
            ':texte' => $texte,
            ':image' => $fileName,
            ':createdAt' => $createdAt
        );

        // 5. On exécute la requête
        $req->execute($datas);
    }
    /**
     * Fonction qui permet d'ajouter du contenu bandeau pour la page conseil
     *
     * @param [type] $post
     * @param [type] $files
     * @return void
     */
    public function addBandeau($post){
        // 1. On traite les données postées
        $titre = addslashes(trim(ucfirst($post['titre'])));
        $soustitre = addslashes(trim(ucfirst($post['soustitre'])));
        $paratexte = addslashes(trim(ucfirst($post['paratexte'])));

        // 3. On fait la requête d'ajout en PDO
        $req = $this->cc->connexion->prepare('INSERT INTO conseils_bandeau (titre, soustitre, paratexte) VALUES (:titre, :soustitre, :paratexte)');

        // 4. On prépare le tableau de données
        $datas = array(
            ':titre' => $titre, 
            ':soustitre' => $soustitre,
            ':paratexte' => $paratexte
        );

        // 5. On exécute la requête
        $req->execute($datas);
    }

    // Fonction getAll pour créer les listes
    /**
     * Fonction qui renvoie toutes les données de la table. Sert pour la fonction list
     *
     * @return void
     */
    public function getArticle(){
        // On écrit la requête
        $req = $this->cc->connexion->query('SELECT * FROM conseils_article', PDO::FETCH_OBJ);
        // On exécute la requête
        $req->execute();
        // On déclare une variable contenant un tableau d'objets ou chaque objet représente un conseil et ses données(colonnes de phpMyAdmin)
        $conseilArticles = $req->fetchAll();
        // On renvoit les données 
        return $conseilArticles;
    }
    /**
     * Fonction qui renvoie toutes les données de la table. Sert pour la fonction list
     *
     * @return void
     */
    public function getBandeau(){
        // On écrit la requête
        $req = $this->cc->connexion->query('SELECT * FROM conseils_bandeau', PDO::FETCH_OBJ);
        // On exécute la requête
        $req->execute();
        // On déclare une variable contenant un tableau d'objets ou chaque objet représente un bandeau et ses données(colonnes de phpMyAdmin)
        $conseilBandeaux = $req->fetchAll();
        // On renvoit les données 
        return $conseilBandeaux;
    }

    // POur l'éditer un bandeau et un article en renvoyant les données sur le formulaire.
    public function getByIdArticle($id){
        // Ecriture de la requête
        $req = $this->cc->connexion->prepare('SELECT * FROM conseils_article WHERE id_article = :id');
        // Execution de la requête
        $req->execute([':id'=>$id]);
        // On renvoie les données
        return $req->fetch();
    }
    public function getByIdBandeau($id){
        // Ecriture de la requête
        $req = $this->cc->connexion->prepare('SELECT * FROM conseils_bandeau WHERE id_bandeau = :id');
        // Execution de la requête
        $req->execute([':id'=>$id]);
        // On renvoie les données
        return $req->fetch();
    }
    public function updateArticle($post, $files){
        // Gestion du fichier image
        $fileName = $post['img-name'];
        if(isset($files['image']['tmp_name']) && !empty($files['image']['tmp_name'])){
            // 1. On récupère l'extension du fichier image à l'aide de pathinfo
            $ext = pathinfo($files['image']['name'], PATHINFO_EXTENSION);
            // 2. On crée un nom unique pour l'image
            $fileName = "conseil_".uniqid().'.'.$ext;
            // 3. On détermine la destination du fichier
            $destination = "assets/images/";
            // 4. On déplace l'image (le fichier temporaire sur le serveur pour le mettre dans le dossier des assets et le renommer à la volée)
            move_uploaded_file($files['image']['tmp_name'], $destination.$fileName);
            // POur des raisons d'espace disque dur sur le serveur, on supprime l'ancienne image avec la méthode php unlink
            unlink($destination.$post['img-name']);
        }

        // Traitement des données
        $titre_article = addslashes(trim(ucfirst($post['titre_article'])));
        $texte = addslashes(trim($post['texte']));
        $createdAt = $post['createdAt'];

        $datas = array(
            ':titre_article' => $titre_article,
            ':texte' => $texte,
            ':image' => $fileName,
            ':createdAt' => $createdAt,
            ':id'=>$post['id']
        );

        // Requête
        $req = $this->cc->connexion->prepare('UPDATE conseils_article SET titre_article=:titre_article, texte=:texte, image=:image, createdAt=:createdAt WHERE id_article=:id');
        // Exécution de la requête
        $req->execute($datas);
    }
    public function updateBandeau($post){
        // Traitement des données
        $titre = addslashes(trim(ucfirst($post['titre'])));
        $soustitre = addslashes(trim(ucfirst($post['soustitre'])));
        $paratexte = addslashes(trim(ucfirst($post['paratexte'])));

        $datas = array(
            ':titre' => $titre, 
            ':soustitre' => $soustitre,
            ':paratexte' => $paratexte,
            ':id' => $post['id']
        );

        // Requête
        $req = $this->cc->connexion->prepare('UPDATE conseils_bandeau SET titre=:titre, soustitre=:soustitre, paratexte=:paratexte WHERE id_bandeau=:id');
        // Exécution de la requête
        $req->execute($datas);
    }

    // Pour supprimer un bandeau ou un article de la BDD.
    public function deleteArticle($id){
        // Suppression du fichier image 
        $conseilArticle = $this->getByIdArticle($id);
        if(!empty($conseilArticle['image'])){
            unlink(SITE_ROOT.'/assets/images/'.$conseilArticle['image']);
        }
        // Suppression de la base de données
        $req = $this->cc->connexion->prepare('DELETE FROM conseils_article WHERE id_article = :id');
        $req->execute([':id'=>$id]);
    }
    public function deleteBandeau($id){
        // Suppression de la base de données
        $req = $this->cc->connexion->prepare('DELETE FROM conseils_bandeau WHERE id_bandeau = :id');
        $req->execute([':id'=>$id]);
    }
}