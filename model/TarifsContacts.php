<?php

namespace OsteoMvc\Model;

use OsteoMvc\Controller\ConnexionController;
use PDO;

class TarifsContacts
{
    // PROPRIETES
    // On déclare une variable qui recevra une instance de ConnexionController pour les transactions avec la BDD.
    private $cc;
    // On déclare des propriétés privées correspondant aux colonnes de nos tables.
    // Bandeau
    private $titre;
    private $soustitre;
    private $paratexte;
    private $banActive;
    // Tarifs
    private $presta;
    private $tarif;
    private $tarActive;
    // Contacts
    private $adresse;
    private $zipcode;
    private $ville;
    private $tel;
    private $mail;
    private $t1;
    private $t2;
    private $t3;
    private $t4;
    private $conActive;
    // Infos Complémentaires
    private $info1;
    private $info2;
    private $info3;
    private $info4;
    private $info5;
    private $info6;
    private $info7;
    private $info8;
    private $info9;
    private $infActive;

    // CONSTRUCTEUR
    public function __construct()
    {
        // On instancie un ConnexionController. 
        $this->cc = new ConnexionController();
    }

    // METHODES

    // Gestion des BANDEAUX

    /**
     * Fonction permettant de poster l'ajout d'un nouveau bandeau et ses contenus.
     *
     * @param [type] $post
     * @return void
     */
    public function addBandeau($post)
    {
        // On traite les données postées ($post).
        // addslashes permettra d'échapper les apostrophes et guillemets.
        // trim permettra d'enlever les espaces au début et à la fin.
        // ucfirst permettra d'assurer une majuscule au premier caractère de la chaîne.
        $titre = addslashes(trim(ucfirst($post['titre'])));
        $soustitre = addslashes(trim(ucfirst($post['soustitre'])));
        $paratexte = addslashes(trim(ucfirst($post['paratexte'])));
        // $banActive = addslashes(trim(ucfirst($post['banActive'])));
        // On fait la requête d'ajout en PDO : il s'agit d'une requête préparée (son avantage étant de prévenir les injections SQL).
        $req = $this->cc->connexion->prepare('INSERT INTO tarifscontacts_bandeau (titre, soustitre, paratexte) VALUES (:titre, :soustitre, :paratexte)');
        // On prépare ensuite un tableau de données contenant les mêmes noms de colonnes que les valeurs attendues dans la requête préparée.
        $datas = array(
            ':titre' => $titre,
            ':soustitre' => $soustitre,
            ':paratexte' => $paratexte,
            // ':banActive' => $banActive
        );
        // Enfin, on exécute la requête.
        $req->execute($datas);
    }

    /**
     * Fonction permettant de poster la modification des contenus d'un bandeau existant.
     *
     * @param [type] $post
     * @return void
     */
    public function updateBandeau($post)
    {
        // On traite les données modifiées et repostées ($post).
        $titre = addslashes(trim(ucfirst($post['titre'])));
        $soustitre = addslashes(trim(ucfirst($post['soustitre'])));
        $paratexte = addslashes(trim(ucfirst($post['paratexte'])));
        // $banActive = addslashes(trim(ucfirst($post['banActive'])));
        // On prépare ensuite un tableau de données contenant les mêmes noms de colonnes que les valeurs attendues dans la requête préparée.
        $datas = array(
            ':titre' => $titre,
            ':soustitre' => $soustitre,
            ':paratexte' => $paratexte,
            // ':banActive' => $banActive,
            ':id' => $post['id']
        );
        // On prépare notre requête.
        $req = $this->cc->connexion->prepare('UPDATE tarifscontacts_bandeau SET titre=:titre, soustitre=:soustitre, paratexte=:paratexte WHERE id=:id');
        // On exécute la requête.
        $req->execute($datas);
    }

    /**
     * Fonction permettant la suppression d'un bandeau (et ses contenus associés) existant.
     *
     * @param [type] $id
     * @return void
     */
    public function deleteBandeau($id)
    {
        // On prépare la requête prenant en charge la suppression du bandeau de la BDD.
        $req = $this->cc->connexion->prepare('DELETE FROM tarifscontacts_bandeau WHERE id=:id');
        // On exécute la requête de suppression.
        $req->execute([':id' => $id]);
    }

    /**
     * Fonction permettant la récupération des contenus de tous les bandeaux en vu d'en afficher la liste complète. 
     *
     * @return void
     */
    public function getAllBandeau()
    {
        // 1 - On crée la requête.
        $req = $this->cc->connexion->query('SELECT * FROM tarifscontacts_bandeau', PDO::FETCH_OBJ);
        // 2 - On exécute la requête.
        $req->execute();
        // 3 - On déclare une variable contenant un tableau composé d'objets (dont chaque objet représente un bandeau et les données qui s'y attachent).
        $tarifscontactsBandeaux = $req->fetchAll();
        // 4 - On renvoie les données.
        return $tarifscontactsBandeaux;
    }

    /**
     * Fonction chargée de récupérer le bandeau à modifier grâce à l'identification de son id. 
     *
     * @param [type] $id
     * @return void
     */
    public function getByIdBandeau($id)
    {
        // Préparation de la requête.
        $req = $this->cc->connexion->prepare('SELECT * FROM tarifscontacts_bandeau WHERE id=:id');
        // Exécution de la requête.
        $req->execute([':id' => $id]);
        // Enfin, on renvoie les données récupérées.
        return $req->fetch();
    }

    // Gestion des TARIFS & PRESTA

    /**
     * Fonction permettant de poster l'ajout d'un nouveau duo presta/tarif.
     *
     * @param [type] $post
     * @return void
     */
    public function addTarif($post)
    {
        // On traite les données postées ($post).
        $presta = addslashes(trim(ucfirst($post['presta'])));
        $tarif = addslashes(trim(ucfirst($post['tarif'])));
        // $tarActive = addslashes(trim(ucfirst($post['tarActive'])));
        // On fait la requête d'ajout en PDO : il s'agit d'une requête préparée (son avantage étant de prévenir les injections SQL).
        $req = $this->cc->connexion->prepare('INSERT INTO tarifscontacts_tarif (presta, tarif) VALUES (:presta, :tarif)');
        // On prépare ensuite un tableau de données contenant les mêmes noms de colonnes que les valeurs attendues dans la requête préparée.
        $datas = array(
            ':presta' => $presta,
            ':tarif' => $tarif,
            // ':banActive' => $banActive
        );
        // Enfin, on exécute la requête.
        $req->execute($datas);
    }

    /**
     * Fonction permettant de poster la modification d'un duo presta/tarif pré-existant.
     *
     * @param [type] $post
     * @return void
     */
    public function updateTarif($post)
    {
        // On traite les données modifiées et repostées ($post).
        $presta = addslashes(trim(ucfirst($post['presta'])));
        $tarif = addslashes(trim(ucfirst($post['tarif'])));
        // $banActive = addslashes(trim(ucfirst($post['banActive'])));
        // On prépare ensuite un tableau de données contenant les mêmes noms de colonnes que les valeurs attendues dans la requête préparée.
        $datas = array(
            ':presta' => $presta,
            ':tarif' => $tarif,
            // ':banActive' => $banActive,
            ':id' => $post['id']
        );
        // On prépare notre requête.
        $req = $this->cc->connexion->prepare('UPDATE tarifscontacts_tarif SET presta=:presta, tarif=:tarif WHERE id=:id');
        // On exécute la requête.
        $req->execute($datas);
    }

    /**
     * Fonction permettant la suppression d'un duo presta/tarif existant.
     *
     * @param [type] $id
     * @return void
     */
    public function deleteTarif($id)
    {
        // On prépare la requête prenant en charge la suppression d'un duo presta/tarif de la BDD.
        $req = $this->cc->connexion->prepare('DELETE FROM tarifscontacts_tarif WHERE id=:id');
        // On exécute la requête de suppression.
        $req->execute([':id' => $id]);
    }

    /**
     * Fonction permettant la récupération des contenus de tous les duos presta/tarif en vu d'en afficher la liste complète. 
     *
     * @return void
     */
    public function getAllTarif()
    {
        // 1 - On crée la requête.
        $req = $this->cc->connexion->query('SELECT * FROM tarifscontacts_tarif', PDO::FETCH_OBJ);
        // 2 - On exécute la requête.
        $req->execute();
        // 3 - On déclare une variable contenant un tableau composé d'objets (dont chaque objet représente un duo presta/tarif et les données qui s'y attachent).
        $tarifscontactsTarifs = $req->fetchAll();
        // 4 - On renvoie les données.
        return $tarifscontactsTarifs;
    }

    /**
     * Fonction chargée de récupérer le duo presta/tarif à modifier grâce à l'identification de son id. 
     *
     * @param [type] $id
     * @return void
     */
    public function getByIdTarif($id)
    {
        // Préparation de la requête.
        $req = $this->cc->connexion->prepare('SELECT * FROM tarifscontacts_tarif WHERE id=:id');
        // Exécution de la requête.
        $req->execute([':id' => $id]);
        // Enfin, on renvoie les données récupérées.
        return $req->fetch();
    }

    // Gestion des CONTACTS
    /**
     * Fonction permettant de publier l'ajout d'un nouveau contact.
     *
     * @param [type] $post
     * @return void
     */
    public function addContact($post)
    {
        // On traite les données postées.
        $adresse = addslashes(trim(ucfirst($post['adresse'])));
        $zipcode = addslashes(trim($post['zipcode']));
        $ville = addslashes(trim(ucfirst($post['ville'])));
        $tel = addslashes(trim($post['tel']));
        $mail = addslashes(trim($post['mail']));
        $t1 = addslashes(trim(ucfirst($post['t1'])));
        $t2 = addslashes(trim(ucfirst($post['t2'])));
        $t3 = addslashes(trim(ucfirst($post['t3'])));
        $t4 = addslashes(trim(ucfirst($post['t4'])));
        // $conActive = addslashes(trim(ucfirst($post['conActive'])));
        // On prépare le tableau de données.
        $datas = array(
            ':adresse' => $adresse,
            ':zipcode' => $zipcode,
            ':ville' => $ville,
            ':tel' => $tel,
            ':mail' => $mail,
            ':t1' => $t1,
            ':t2' => $t2,
            ':t3' => $t3,
            ':t4' => $t4,
            // ':conActive' => $conActive
        );
        // On prépare la requête.
        $req = $this->cc->connexion->prepare('INSERT INTO tarifscontacts_contact (adresse, zipcode, ville, tel, mail, t1, t2, t3, t4) VALUES (:adresse, :zipcode, :ville, :tel, :mail, :t1, :t2, :t3, :t4)');
        // On exécute la requête.
        $req->execute($datas);
    }

    /**
     * Fonction permettant de publier la modification d'un contact pré-existant.
     *
     * @param [type] $post
     * @return void
     */
    public function updateContact($post)
    {
        // On traite les données postées.
        $adresse = addslashes(trim(ucfirst($post['adresse'])));
        $zipcode = addslashes(trim($post['zipcode']));
        $ville = addslashes(trim(ucfirst($post['ville'])));
        $tel = addslashes(trim($post['tel']));
        $mail = addslashes(trim($post['mail']));
        $t1 = addslashes(trim(ucfirst($post['t1'])));
        $t2 = addslashes(trim(ucfirst($post['t2'])));
        $t3 = addslashes(trim(ucfirst($post['t3'])));
        $t4 = addslashes(trim(ucfirst($post['t4'])));
        // $conActive = addslashes(trim(ucfirst($post['conActive'])));
        // On prépare le tableau de données.
        $datas = array(
            ':adresse' => $adresse,
            ':zipcode' => $zipcode,
            ':ville' => $ville,
            ':tel' => $tel,
            ':mail' => $mail,
            ':t1' => $t1,
            ':t2' => $t2,
            ':t3' => $t3,
            ':t4' => $t4,
            // ':conActive' => $conActive,
            ':id' => $post['id']
        );
        // On prépare la requête.
        $req = $this->cc->connexion->prepare('UPDATE tarifscontacts_contact SET adresse=:adresse, zipcode=:zipcode, ville=:ville, tel=:tel, mail=:mail, t1=:t1, t2=:t2, t3=:t3, t4=:t4 WHERE id=:id');
        // On exécute la requête.
        $req->execute($datas);
    }

    /**
     * Fonction permettant de supprimer un contact existant et ses contenus de la BDD.
     *
     * @param [type] $id
     * @return void
     */
    public function deleteContact($id)
    {
        // On prépare la requête.
        $req = $this->cc->connexion->prepare('DELETE FROM tarifscontacts_contact WHERE id=:id');
        // On exécute la requête.
        $req->execute([':id' => $id]);
    }

    /**
     * Fonction permettant la récupération des contenus de tous les contacts en vu d'en afficher la liste complète. 
     *
     * @return void
     */
    public function getAllContact()
    {
        // On crée la requête.
        $req = $this->cc->connexion->query('SELECT * FROM tarifscontacts_contact', PDO::FETCH_OBJ);
        // On exécute la requête.
        $req->execute();
        // On déclare une variable contenant un tableau composé d'objets.
        $tarifscontactsContacts = $req->fetchAll();
        // On renvoie les données.
        return $tarifscontactsContacts;
    }

    /**
     * Fonction chargée de récupérer le contact à modifier grâce à l'identification de son id. 
     *
     * @param [type] $id
     * @return void
     */
    public function getByIdContact($id)
    {
        // On prépare la requête.
        $req = $this->cc->connexion->prepare('SELECT * FROM tarifscontacts_contact WHERE id=:id');
        // On exécute la requête.
        $req->execute([':id' => $id]);
        // On renvoie les données récupérées.
        return $req->fetch();
    }

    // Gestion des INFOS COMPLEMENTAIRES

    /**
     * Fonction permettant de publier l'ajout d'un nouveau groupe d'infos complémentaires.
     *
     * @return void
     */
    public function addInfoComp($post)
    {
        // Traitement des données publiées.
        $info1 = addslashes(trim(ucfirst($post['info1'])));
        $info2 = addslashes(trim(ucfirst($post['info2'])));
        $info3 = addslashes(trim(ucfirst($post['info3'])));
        $info4 = addslashes(trim(ucfirst($post['info4'])));
        $info5 = addslashes(trim(ucfirst($post['info5'])));
        // $infActive = addslashes(trim(ucfirst($post['infActive'])));
        // Préparation du tableau de données associé.
        $datas = array(
            ':info1' => $info1,
            ':info2' => $info2,
            ':info3' => $info3,
            ':info4' => $info4,
            ':info5' => $info5,
            // ':infActive' => $infActive
        );
        // Préparation de la requête.
        $req = $this->cc->connexion->prepare('INSERT INTO tarifscontacts_infocomp (info1, info2, info3, info4, info5) VALUES (:info1, :info2, :info3, :info4, :info5)');
        // Exécution de la requête.
        $req->execute($datas);
    }

    public function updateInfoComp($post)
    {
        // Traitement des données publiées.
        $info1 = addslashes(trim(ucfirst($post['info1'])));
        $info2 = addslashes(trim(ucfirst($post['info2'])));
        $info3 = addslashes(trim(ucfirst($post['info3'])));
        $info4 = addslashes(trim(ucfirst($post['info4'])));
        $info5 = addslashes(trim(ucfirst($post['info5'])));
        // $infActive = addslashes(trim(ucfirst($post['infActive'])));
        // Préparation du tableau de données associé.
        $datas = array(
            ':info1' => $info1,
            ':info2' => $info2,
            ':info3' => $info3,
            ':info4' => $info4,
            ':info5' => $info5,
            // ':infActive' => $infActive
            ':id' => $post['id']
        );
        // Préparation de la requête.
        $req = $this->cc->connexion->prepare('UPDATE tarifscontacts_infocomp SET info1=:info1, info2=:info2, info3=:info3, info4=:info4, info5=:info5 WHERE id=:id');
        // Exécution de la requête.
        $req->execute($datas);
    }

    public function deleteInfoComp($id)
    {
        // On prépare la requête.
        $req = $this->cc->connexion->prepare('DELETE FROM tarifscontacts_infocomp WHERE id=:id');
        // On exécute la requête.
        $req->execute([':id' => $id]);
    }

    public function getAllInfoComp()
    {
        // On crée la requête.
        $req = $this->cc->connexion->query('SELECT * FROM tarifscontacts_infocomp', PDO::FETCH_OBJ);
        // On exécute la requête.
        $req->execute();
        // On déclare une variable contenant un tableau composé d'objets.
        $tarifscontactsInfosComps = $req->fetchAll();
        // On renvoie les données.
        return $tarifscontactsInfosComps;
    }

    public function getByIdInfoComp($id)
    {
        // On prépare la requête.
        $req = $this->cc->connexion->prepare('SELECT * FROM tarifscontacts_infocomp WHERE id=:id');
        // On exécute la requête.
        $req->execute(['id' => $id]);
        // On renvoie les données récupérées.
        return $req->fetch();
    }

    // GETTERS / SETTERS

}

// CODE DALILA 

// /**
//      * Fonction qui renvoie toutes les données de la table. Sert pour la fonction list
//      *
//      * @return void
//      */
//     public function getArticle(){
//         // On écrit la requête
        // $req = $this->cc->connexion->query('SELECT * FROM conseils_article', PDO::FETCH_OBJ);
//         // On exécute la requête
//         $req->execute();
//         // On déclare une variable contenant un tableau d'objets ou chaque objet représente un conseil et ses données(colonnes de phpMyAdmin)
//         $conseilArticles = $req->fetchAll();
//         // On renvoit les données 
//         return $conseilArticles;
//     }
