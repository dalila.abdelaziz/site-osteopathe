<?php

namespace App\Model;

use App\Controller\ConnexionController;
use PDO;

class Home{
    // Propriétés
    // On déclare des propriétés privées correspondant aux colonnes de la table
    private $titre_1;
    private $titre_2;
    private $titre_3;
    private $titre_4;
    private $texte_bandeau;
    private $image_presentation;
    // Je déclare une variable pour le fichier physique de l'image
    private $presentationFile;
    private $texte_presentation;
    private $adresse;
    private $telephone;
    private $email;
    private $jour_ouverture;
    private $heure_ouverture;
    private $heure_fermeture;
    private $publik;
    private $image_publik;
    // Je déclare une variable pour le fichier physique de l'image
    private $publikFile;
    private $pathologie;
    private $image_patho;
    // Je déclare une variable pour le fichier physique de l'image
    private $pathoFile;
    // On déclare une variable qui recevra une instance de ConnexionController pour les transactions avec la BDD
    private $cc;

    // Constructeur
    public function __construct()
    {
        // On instancie un ConnexionController
        $this->cc = new ConnexionController();
    }

    // Méthodes
    // Fonction getAll pour créer les listes
    public function getBandeauHome(){
        // On écrit la requête
        $req = $this->cc->connexion->query('SELECT * FROM home_bandeau', PDO::FETCH_OBJ);
        // On exécute la requête
        $req->execute();
        // On déclare une variable contenant un tableau d'objets ou chaque objet représente un bandeau et ses données(colonnes de phpMyAdmin)
        $home_bandeaux = $req->fetchAll();
        // On renvoie les données
        return $home_bandeaux;
    }
    public function getPresentation(){
        // On écrit la requête
        $req = $this->cc->connexion->query('SELECT * FROM home_presentation', PDO::FETCH_OBJ);
        // On exécute la requête
        $req->execute();
        // On déclare une variable contenant un tableau d'objets ou chaque objet représente une présentation et ses données(colonnes de phpMyAdmin)
        $home_presentations = $req->fetchAll();
        // On renvoie les données
        return $home_presentations;
    }
    public function getContactHome(){
        // On écrit la requête
        $req = $this->cc->connexion->query('SELECT * FROM home_contact', PDO::FETCH_OBJ);
        // On exécute la requête
        $req->execute();
        // On déclare une variable contenant un tableau d'objets ou chaque objet représente un contact et ses données(colonnes de phpMyAdmin)
        $home_contacts = $req->fetchAll();
        // On renvoie les données
        return $home_contacts;
    }
    public function getHoraireHome(){
        // On écrit la requête
        $req = $this->cc->connexion->query('SELECT * FROM home_horaire', PDO::FETCH_OBJ);
        // On exécute la requête
        $req->execute();
        // On déclare une variable contenant un tableau d'objets ou chaque objet représente un contact et ses données(colonnes de phpMyAdmin)
        $home_horaires = $req->fetchAll();
        // On renvoie les données
        return $home_horaires;
    }
    public function getPublik(){
        // On écrit la requête
        $req = $this->cc->connexion->query('SELECT * FROM home_public', PDO::FETCH_OBJ);
        // On exécute la requête
        $req->execute();
        // On déclare une variable contenant un tableau d'objets ou chaque objet représente un contact et ses données(colonnes de phpMyAdmin)
        $home_publiks = $req->fetchAll();
        // On renvoie les données
        return $home_publiks;
    }
    public function getPatho(){
        // On écrit la requête
        $req = $this->cc->connexion->query('SELECT * FROM home_patho', PDO::FETCH_OBJ);
        // On exécute la requête
        $req->execute();
        // On déclare une variable contenant un tableau d'objets ou chaque objet représente un contact et ses données(colonnes de phpMyAdmin)
        $home_pathos = $req->fetchAll();
        // On renvoie les données
        return $home_pathos;
    }

    // Fonctions qui prennent en charge l'ajout en base de données
    public function addBandeauHome($post){
        // 1. Traitement des données postées
        $titre_1 = addslashes(trim(ucfirst($post['titre_1'])));
        $titre_2 = addslashes(trim(ucfirst($post['titre_2'])));
        $titre_3 = addslashes(trim(ucfirst($post['titre_3'])));
        $titre_4 = addslashes(trim(ucfirst($post['titre_4'])));
        $texte_bandeau = addslashes(trim($post['texte_bandeau']));

        // 2. Ecriture de la requête
        $req = $this->cc->connexion->prepare('INSERT INTO home_bandeau (titre_1, titre_2, titre_3, titre_4, texte_bandeau) VALUES (:titre_1, :titre_2, :titre_3, :titre_4, :texte_bandeau)');

        // 3. Préparation du tableau de données
        $datas = array(
            ':titre_1' => $titre_1,
            ':titre_2' => $titre_2,
            ':titre_3' => $titre_3,
            ':titre_4' => $titre_4,
            ':texte_bandeau' => $texte_bandeau
        );

        // 4. Exécution de la requête
        $req->execute($datas);
    }
    public function addPresentation($post, $files){
        // 1. Traitement du fichier image
        $presentationFile = "";
        if(isset($files['image']['tmp_name'])){
            // On renomme l'image
            // 1. On récupère l'extension du fichier image à l'aide de pathinfo
            $ext = pathinfo($files['image']['name'], PATHINFO_EXTENSION);
            // 2. On crée un nom unique pour l'image
            $presentationFile = "presentation_".uniqid().'.'.$ext;
            // 3. On détermine la destination du fichier
            $destination = "assets/images/";
            // 4. On déplace l'image (le fichier temporaire sur le serveur pour le mettre dans le dossier des assets et le renommer à la volée)
            move_uploaded_file($files['image']['tmp_name'], $destination.$presentationFile);
        }

        // 2. Traitement des données postées
        $texte_presentation = addslashes(trim($post['texte_presentation']));

        // 3. Ecriture de la requête
        $req = $this->cc->connexion->prepare('INSERT INTO home_presentation (image_presentation, texte_presentation) VALUES (:image, :texte_presentation)');

        // 4. Préparation du tableau de données
        $datas = array(
            ':image' => $presentationFile,
            ':texte_presentation' => $texte_presentation,
        );

        // 5. Exécution de la requête
        $req->execute($datas);
    }
    public function addContactHome($post){
        // 1. Traitement des données postées
        $adresse = addslashes(trim(ucfirst($post['adresse'])));
        $telephone = trim($post['telephone']);
        $email = trim($post['email']);

        // 2. Ecriture de la requête
        $req = $this->cc->connexion->prepare('INSERT INTO home_contact (adresse, telephone, email) VALUES (:adresse, :telephone, :email)');

        // 3. Préparation du tableau de données
        $datas = array(
            ':adresse' => $adresse,
            ':telephone' => $telephone,
            ':email' => $email
        );

        // 4. Exécution de la requête
        $req->execute($datas);
    }
    public function addHoraireHome($post){
        // 1. Traitement des données postées
        $jour_ouverture = trim(ucfirst($post['jour_ouverture']));
        $heure_ouverture = trim($post['heure_ouverture']);
        $heure_fermeture = trim($post['heure_fermeture']);

        // 2. Ecriture de la requête
        $req = $this->cc->connexion->prepare('INSERT INTO home_horaire (jour_ouverture, heure_ouverture, heure_fermeture) VALUES (:jour_ouverture, :heure_ouverture, :heure_fermeture)');

        // 3. Préparation du tableau de données
        $datas = array(
            ':jour_ouverture' => $jour_ouverture,
            ':heure_ouverture' => $heure_ouverture,
            ':heure_fermeture' => $heure_fermeture
        );

        // 4. Exécution de la requête
        $req->execute($datas);
    }
    public function addPublik($post, $files){
        // 1. Traitement du fichier image
        $publikFile = "";
        if(isset($files['image']['tmp_name'])){
            // On renomme l'image
            // 1. On récupère l'extension du fichier image à l'aide de pathinfo
            $ext = pathinfo($files['image']['name'], PATHINFO_EXTENSION);
            // 2. On crée un nom unique pour l'image
            $publikFile = "publik_".uniqid().'.'.$ext;
            // 3. On détermine la destination du fichier
            $destination = "assets/images/";
            // 4. On déplace l'image (le fichier temporaire sur le serveur pour le mettre dans le dossier des assets et le renommer à la volée)
            move_uploaded_file($files['image']['tmp_name'], $destination.$publikFile);
        }

        // 2. Traitement des données postées
        $publik = addslashes(trim(ucfirst($post['publik'])));

        // 3. Ecriture de la requête
        $req = $this->cc->connexion->prepare('INSERT INTO home_public (publik, image_publik) VALUES (:publik, :image)');

        // 4. Préparation du tableau de données
        $datas = array(
            ':publik' => $publik,
            ':image' => $publikFile
        );

        // 5. Exécution de la requête
        $req->execute($datas);
    }
    public function addPatho($post, $files){
        // 1. Traitement du fichier image
        $pathoFile = "";
        if(isset($files['image']['tmp_name'])){
            // On renomme l'image
            // 1. On récupère l'extension du fichier image à l'aide de pathinfo
            $ext = pathinfo($files['image']['name'], PATHINFO_EXTENSION);
            // 2. On crée un nom unique pour l'image
            $pathoFile = "pathologie_".uniqid().'.'.$ext;
            // 3. On détermine la destination du fichier
            $destination = "assets/images/";
            // 4. On déplace l'image (le fichier temporaire sur le serveur pour le mettre dans le dossier des assets et le renommer à la volée)
            move_uploaded_file($files['image']['tmp_name'], $destination.$pathoFile);
        }

        // 2. Traitement des données postées
        $pathologie = addslashes(trim(ucfirst($post['pathologie'])));

        // 3. Ecriture de la requête
        $req = $this->cc->connexion->prepare('INSERT INTO home_patho (pathologie, image_patho) VALUES (:pathologie, :image)');

        // 4. Préparation du tableau de données
        $datas = array(
            ':pathologie' => $pathologie,
            ':image' => $pathoFile
        );

        // 5. Exécution de la requête
        $req->execute($datas);
    }

    // Fonctions qui récupèrent un élément par son id
    public function getByIdBandeauHome($id){
        // Ecriture de la requête
        $req = $this->cc->connexion->prepare('SELECT * FROM home_bandeau WHERE id_bandeau = :id');
        // Execution de la requête
        $req->execute([':id'=>$id]);
        // On renvoie les données
        return $req->fetch();
    }
    public function getByIdPresentation($id){
        // Ecriture de la requête
        $req = $this->cc->connexion->prepare('SELECT * FROM home_presentation WHERE id_presentation = :id');
        // Exécution de la requête
        $req->execute([':id'=>$id]);
        // On renvoie les données
        return $req->fetch();
    }
    public function getByIdContactHome($id){
        // Ecriture de la requête
        $req = $this->cc->connexion->prepare('SELECT * FROM home_contact WHERE id_contact = :id');
        // Exécution de la requête
        $req->execute([':id'=>$id]);
        // On renvoie les données
        return $req->fetch();
    }
    public function getByIdHoraireHome($id){
        // Ecriture de la requête
        $req = $this->cc->connexion->prepare('SELECT * FROM home_horaire WHERE id_horaire = :id');
        // Exécution de la requête
        $req->execute([':id'=>$id]);
        // On renvoie les données
        return $req->fetch();
    }
    public function getByIdPublik($id){
        // Ecriture de la requête
        $req = $this->cc->connexion->prepare('SELECT * FROM home_public WHERE id_publik = :id');
        // Exécution de la requête
        $req->execute([':id'=>$id]);
        // On renvoie les données
        return $req->fetch();
    }
    public function getByIdPatho($id){
        // Ecriture de la requête
        $req = $this->cc->connexion->prepare('SELECT * FROM home_patho WHERE id_patho = :id');
        // Exécution de la requête
        $req->execute([':id'=>$id]);
        // On renvoie les données
        return $req->fetch();
    }

    // Fonctions qui envoie le formulaire d'update
    public function updateBandeauHome($post){
        // 1. Traitement des données postées
        $titre_1 = addslashes(trim(ucfirst($post['titre_1'])));
        $titre_2 = addslashes(trim(ucfirst($post['titre_2'])));
        $titre_3 = addslashes(trim(ucfirst($post['titre_3'])));
        $titre_4 = addslashes(trim(ucfirst($post['titre_4'])));
        $texte_bandeau = addslashes(trim($post['texte_bandeau']));

        // 2. Préparation du tableau de données
        $datas = array(
            ':titre_1' => $titre_1,
            ':titre_2' => $titre_2,
            ':titre_3' => $titre_3,
            ':titre_4' => $titre_4,
            ':texte_bandeau' => $texte_bandeau,
            ':id' => $post['id']
        );

        // 3. Ecriture de la requête
        $req = $this->cc->connexion->prepare('UPDATE home_bandeau SET titre_1=:titre_1, titre_2=:titre_2, titre_3=:titre_3, titre_4=:titre_4, texte_bandeau=:texte_bandeau WHERE id_bandeau=:id');

        // 4. Exécution de la requête
        $req->execute($datas);
    }
    public function updatePresentation($post, $files){
        // 1. Traitement du fichier image
        $presentationFile = $post['img-name'];
        if(isset($files['image']['tmp_name']) && !empty($files['image']['tmp_name'])){
            // 1. On récupère l'extension du fichier image à l'aide de pathinfo
            $ext = pathinfo($files['image']['name'], PATHINFO_EXTENSION);
            // 2. On crée un nom unique pour l'image
            $presentationFile = "presentation_".uniqid().'.'.$ext;
            // 3. On détermine la destination du fichier
            $destination = "assets/images/";
            // 4. On déplace l'image (le fichier temporaire sur le serveur pour le mettre dans le dossier des assets et le renommer à la volée)
            move_uploaded_file($files['image']['tmp_name'], $destination.$presentationFile);
            // POur des raisons d'espace disque dur sur le serveur, on supprime l'ancienne image avec la méthode php unlink
            unlink($destination.$post['img-name']);
        }

        // 2. Traitement des données postées
        $texte_presentation = addslashes(trim($post['texte_presentation']));

        // 3. Préparation du tableau de données
        $datas = array(
            ':image' => $presentationFile,
            ':texte_presentation' => $texte_presentation,
            ':id'=>$post['id']
        );

        // 4. Ecriture de la requête
        $req = $this->cc->connexion->prepare('UPDATE home_presentation SET image_presentation=:image, texte_presentation=:texte_presentation WHERE id_presentation=:id');

        // 5. Exécution de la requête
        $req->execute($datas);
    }
    public function updateContactHome($post){
        // 1. Traitement des données postées
        $adresse = addslashes(trim(ucfirst($post['adresse'])));
        $telephone = trim($post['telephone']);
        $email = trim($post['email']);

        // 2. Préparation du tableau de données
        $datas = array(
            ':adresse' => $adresse,
            ':telephone' => $telephone,
            ':email' => $email,
            ':id' => $post['id']
        );

        // 3. Ecriture de la requête
        $req = $this->cc->connexion->prepare('UPDATE home_contact SET adresse=:adresse, telephone=:telephone, email=:email WHERE id_contact=:id');

        // 4. Exécution de la requête
        $req->execute($datas);
    }
    public function updateHoraireHome($post){
        // 1. Traitement des données postées
        $jour_ouverture = trim(ucfirst($post['jour_ouverture']));
        $heure_ouverture = trim($post['heure_ouverture']);
        $heure_fermeture = trim($post['heure_fermeture']);

        // 2. Préparation du tableau de données
        $datas = array(
            ':jour_ouverture' => $jour_ouverture,
            ':heure_ouverture' => $heure_ouverture,
            ':heure_fermeture' => $heure_fermeture,
            ':id' => $post['id']
        );

        // 3. Ecriture de la requête
        $req = $this->cc->connexion->prepare('UPDATE home_horaire SET jour_ouverture=:jour_ouverture, heure_ouverture=:heure_ouverture, heure_fermeture=:heure_fermeture WHERE id_horaire=:id');

        // 4. Exécution de la requête
        $req->execute($datas);
    }
    public function updatePublik($post, $files){
        // 1. Traitement du fichier image
        $publikFile = $post['img-name'];
        if(isset($files['image']['tmp_name']) && !empty($files['image']['tmp_name'])){
            // 1. On récupère l'extension du fichier image à l'aide de pathinfo
            $ext = pathinfo($files['image']['name'], PATHINFO_EXTENSION);
            // 2. On crée un nom unique pour l'image
            $publikFile = "publik_".uniqid().'.'.$ext;
            // 3. On détermine la destination du fichier
            $destination = "assets/images/";
            // 4. On déplace l'image (le fichier temporaire sur le serveur pour le mettre dans le dossier des assets et le renommer à la volée)
            move_uploaded_file($files['image']['tmp_name'], $destination.$publikFile);
            // POur des raisons d'espace disque dur sur le serveur, on supprime l'ancienne image avec la méthode php unlink
            unlink($destination.$post['img-name']);
        }

        // 2. Traitement des données postées
        $publik = addslashes(trim(ucfirst($post['publik'])));

        // 3. Préparation du tableau de données
        $datas = array(
            ':publik' => $publik,
            ':image' => $publikFile,
            ':id' => $post['id']
        );

        // 4. Ecriture de la requête
        $req = $this->cc->connexion->prepare('UPDATE home_public SET publik=:publik, image_publik=:image WHERE id_publik=:id');

        // 5. Exécution de la requête
        $req->execute($datas);
    }
    public function updatePatho($post, $files){
        // 1. Traitement du fichier image
        $pathoFile = $post['img-name'];
        if(isset($files['image']['tmp_name']) && !empty($files['image']['tmp_name'])){
            // 1. On récupère l'extension du fichier image à l'aide de pathinfo
            $ext = pathinfo($files['image']['name'], PATHINFO_EXTENSION);
            // 2. On crée un nom unique pour l'image
            $pathoFile = "pathologie_".uniqid().'.'.$ext;
            // 3. On détermine la destination du fichier
            $destination = "assets/images/";
            // 4. On déplace l'image (le fichier temporaire sur le serveur pour le mettre dans le dossier des assets et le renommer à la volée)
            move_uploaded_file($files['image']['tmp_name'], $destination.$pathoFile);
            // POur des raisons d'espace disque dur sur le serveur, on supprime l'ancienne image avec la méthode php unlink
            unlink($destination.$post['img-name']);
        }

        // 2. Traitement des données postées
        $pathologie = addslashes(trim(ucfirst($post['pathologie'])));

        // 3. Préparation du tableau de données
        $datas = array(
            ':pathologie' => $pathologie,
            ':image' => $pathoFile,
            ':id' => $post['id']
        );

        // 4. Ecriture de la requête
        $req = $this->cc->connexion->prepare('UPDATE home_patho SET pathologie=:pathologie, image_patho=:image WHERE id_patho=:id');

        // 5. Exécution de la requête
        $req->execute($datas);
    }
    // Fonctions qui prennent en charge la suppression de données
    public function deleteBandeauHome($id){
        // Suppression de la base de données
        $req = $this->cc->connexion->prepare('DELETE FROM home_bandeau WHERE id_bandeau = :id');
        $req->execute([':id'=>$id]);
    }
    public function deletePresentation($id){
        // Suppression du fichier image
        $home_presentation = $this->getByIdPresentation($id);
        if(!empty($home_presentation['image_presentation'])){
            unlink(SITE_ROOT.'/assets/images/'.$home_presentation['image_presentation']);
        }
        // Suppression de la base de données
        $req = $this->cc->connexion->prepare('DELETE FROM home_presentation WHERE id_presentation = :id');
        $req->execute([':id'=>$id]);
    }
    public function deleteContactHome($id){
        // Suppression de la base de données
        $req = $this->cc->connexion->prepare('DELETE FROM home_contact WHERE id_contact = :id');
        $req->execute([':id'=>$id]);
    }
    public function deleteHoraireHome($id){
        // Suppression de la base de données
        $req = $this->cc->connexion->prepare('DELETE FROM home_horaire WHERE id_horaire = :id');
        $req->execute([':id'=>$id]);
    }
    public function deletePublik($id){
        // Suppression du fichier image
        $home_publik = $this->getByIdPublik($id);
        if(!empty($home_publik['image_publik'])){
            unlink(SITE_ROOT.'/assets/images/'.$home_publik['image_publik']);
        }
        // Suppression de la base de données
        $req = $this->cc->connexion->prepare('DELETE FROM home_public WHERE id_publik = :id');
        $req->execute([':id'=>$id]);
    }
    public function deletePatho($id){
        // Suppression du fichier image
        $home_patho = $this->getByIdPatho($id);
        if(!empty($home_patho['image_patho'])){
            unlink(SITE_ROOT.'/assets/images/'.$home_patho['image_patho']);
        }
        // Suppression de la base de données
        $req = $this->cc->connexion->prepare('DELETE FROM home_patho WHERE id_patho = :id');
        $req->execute([':id'=>$id]);
    }
}