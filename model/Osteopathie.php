<?php
// On met en place un namespace qui représente une manière abstraite de ranger les fichiers. Càd que l'on crée des dossiers virtuels auxquels sont associés les fichiers.
// Les namespaces permettent ensuite l'utilisation de use.
namespace OsteoMvc\Model;

use OsteoMvc\Controller\ConnexionController;
use PDO;

class Osteopathie
{
    // PROPRIETES
    // On déclare une variable qui recevra une instance de ConnexionController pour les transactions avec la BDD.
    private $cc;
    // On déclare des propriétés privées correspondant aux colonnes de nos tables.
    // Propriétés nécessaires aux indications
    private $image;
    // On intègre ici une variable pour le fichier physique de l'image.
    private $imageFile;
    private $titre;
    private $texte;
    private $oIndActive;

    // CONSTRUCTEUR
    public function __construct()
    {
        // On instancie un ConnexionController.
        $this->cc = new ConnexionController();
    }

    // METHODES
    // Gestion des INDICATIONS
    public function addIndication($post, $files)
    {
        // 1 - Tout d'abord on traite le fichier image puisqu'il y en aura un.
        $fileName = "";
        // Ensuite on vérifie si une image est envoyée.
        if (isset($files['image']['tmp_name'])) {
            // Si oui, alors on renomme l'image.
            // 1 - On récupère ici l'extension du fichier image via pathinfo.
            $ext = pathinfo($files['image']['name'], PATHINFO_EXTENSION);
            // 2 - On crée un nom unique pour l'image que l'on stocke dans $fileName. 
            $fileName = "indication_" . uniqid() . '.' . $ext;
            // 3 - On détermine la destination du fichier.
            $destination = "assets/images/";
            // 4 - Enfin on déplace l'image : le fichier temporaire sur le serveur pour le mettre dans le dossier des assets et le renommer à la volée.
            move_uploaded_file($files['image']['tmp_name'], $destination . $fileName);
        }
        // 2 - Ensuite on peut traiter les données postées.
        $titre = addslashes(trim(ucfirst($post['titre'])));
        $texte = addslashes(trim(ucfirst($post['texte'])));
        // $oIndActive = addslashes(trim(ucfirst($post['oIndActive'])));
        // 3 - Puis on peut préparer la requête d'ajout en PDO.
        $req = $this->cc->connexion->prepare('INSERT INTO osteopathie_indication (image, titre, texte) VALUES (:image, :titre, :texte)');
        // 4 - On prépare ici notre tableau de données qui contient les mêmes noms de colonnes que les valeurs attendues dans la requête préparée.
        $datas = array(
            ':image' => $fileName,
            ':titre' => $titre,
            ':texte' => $texte,
            // ':oIndActive' => $oIndActive
        );
        // 5 - Exécution de la requête.
        $req->execute($datas);
    }

    public function updateIndication($post, $files)
    {
        // 1 - Tout d'abord on traite le fichier image puisqu'il y en aura un.
        $fileName = $post['img-name'];
        // Ensuite on vérifie si une image est envoyée.
        if (isset($files['image']['tmp_name']) && !empty($files['image']['tmp_name'])) {
            // Si oui, alors on renomme l'image.
            // 1 - On récupère ici l'extension du fichier image via pathinfo.
            $ext = pathinfo($files['image']['name'], PATHINFO_EXTENSION);
            // 2 - On crée un nom unique pour l'image que l'on stocke dans $fileName. 
            $fileName = "indication_" . uniqid() . '.' . $ext;
            // 3 - On détermine la destination du fichier.
            $destination = "assets/images/";
            // 4 - Enfin on déplace l'image : le fichier temporaire sur le serveur pour le mettre dans le dossier des assets et le renommer à la volée.
            move_uploaded_file($files['image']['tmp_name'], $destination . $fileName);
            // Pour des raisons d'espace disque sur le serveur, on supprime l'ancienne image via la méthode php unlink.
            unlink($destination . $post['img-name']);
        }
        // 2 - Ensuite on peut traiter les données postées.
        $titre = addslashes(trim(ucfirst($post['titre'])));
        $texte = addslashes(trim(ucfirst($post['texte'])));
        // $oIndActive = addslashes(trim(ucfirst($post['oIndActive'])));
        // 3 - Puis on peut préparer la requête d'ajout en PDO.
        $req = $this->cc->connexion->prepare('UPDATE osteopathie_indication SET image=:image, titre=:titre, texte=:texte WHERE id=:id');
        // 4 - On prépare ici notre tableau de données qui contient les mêmes noms de colonnes que les valeurs attendues dans la requête préparée.
        $datas = array(
            ':image' => $fileName,
            ':titre' => $titre,
            ':texte' => $texte,
            // ':oIndActive' => $oIndActive,
            ':id' => $post['id']
        );
        // 5 - Exécution de la requête.
        $req->execute($datas);
    }

    public function deleteIndication($id)
    {
        // Suppression de l'image associée.
        $osteopathieIndication = $this->getByIdIndication($id);
        if (!empty($osteopathieIndication["image"])) {
            unlink(SITE_ROOT . '/assets/images/' . $osteopathieIndication["image"]);
        }
        // On prépare la requête pour supprimer l'indication de la BDD.
        $req = $this->cc->connexion->prepare('DELETE FROM osteopathie_indication WHERE id=:id');
        // On exécute la requête.
        $req->execute([':id' => $id]);
    }

    public function getAllIndication()
    {
        // 1 - On crée la requête.
        $req = $this->cc->connexion->query('SELECT * FROM osteopathie_indication', PDO::FETCH_OBJ);
        // 2 - On exécute la requête.
        $req->execute();
        // 3 - On déclare une variable contenant un tableau composé d'objets.
        $osteopathieIndications = $req->fetchAll();
        // 4 - On renvoie les données.
        return $osteopathieIndications;
    }

    public function getByIdIndication($id)
    {
        // Préparation de la requête.
        $req = $this->cc->connexion->prepare('SELECT * FROM osteopathie_indication WHERE id=:id');
        // Exécution de la requête.
        $req->execute([':id' => $id]);
        // Renvoie des données récupérées.
        return $req->fetch();
    }

    // GETTERS / SETTERS

}
