<?php
// Si la session n'est pas déjà lancée, on la démarre.
if (!isset($_SESSION)) session_start();
?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <!-- On gère ici les méta données, les CDN's et les links CSS via l'include du fichier admin-head.inc.php -->
    <?php include('view/inc/admin-head.inc.php'); ?>
    <title>Admin Gestion Tarifs & Contact</title>
</head>

<body class="bg-light">
    <!-- HEADER -->
    <!-- On gère le header développé dans un autre fichier (CF admin-header.inc.php) via un include. -->
    <?php include('view/inc/admin-header.inc.php'); ?>
    <!-- FIN HEADER -->
    <!-- BANDEAU BACK OSTEOPATHIE -->
    <div class="container-fluid bandeau">
        <h1 class="my-5">Espace Administrateur</h1>
        <hr>
        <h2 class="my-5">L'ostéopathie - Anecdotes - Indications</h2>
        <hr>
        <h5 class="my-5">Bienvenue sur votre back-office. D'ici, vous pouvez gérer le contenu de l'intégralité de votre page L'ostéopathie.</h5>
    </div>
    <!-- FIN BANDEAU BACK OSTEOPATHIE -->
    <!-- CONTAINER PRINCIPAL englobant le reste du contenu attendu sur cette page : listes et formulaires d'ajout -->
    <div class="container-fluid py-3">
        <!-- BLOC INDICATION -->
        <div class="p-4 my-3 shadow bg-dark">
            <?php
            if (isset($_SESSION['message-indication'])) {
                echo '<div class="alert alert-success">' . $_SESSION['message-indication'] . '</div>';
                unset($_SESSION['message-indication']);
            }
            ?>
            <!-- Liste -->
            <h3>Liste des indications :</h3>
            <!-- On crée ici une table nous permettant l'affichage statique (noms des champs) et dynamique (contenus renseignés) des données validées dans un formulaire -->
            <table class="table table-borderless">
                <!-- thead correspond à la partie de la table attendant les données d'en tête -->
                <thead class="text-center">
                    <!-- tr correspond à la création d'une ligne attendant du contenu -->
                    <tr>
                        <!-- th correspond aux contenus renseignés dans la ligne de l'en-tête de la table (soit les intitulés des champs à renseigner) -->
                        <th scope="col">Image</th>
                        <th scope="col">Titre</th>
                        <th scope="col">Texte</th>
                        <th scope="col">Active</th>
                        <th scope="col">Modifier</th>
                        <th scope="col">Supprimer</th>
                    </tr>
                </thead>
                <!-- tbody correspond à la partie de la table attendant les données renseignées lors de l'ajout du formulaire (contenus des champs) -->
                <tbody class="text-center">
                    <?php foreach ($osteopathieIndications as $osteopathieIndication) { ?>
                        <tr>
                            <!-- td correspond ici aux datas renseignées pour chaque champ -->
                            <td><img src="<?php echo BASE_FOLDER; ?>/assets/images/<?php echo $osteopathieIndication->image; ?>" alt="" class="img-fluid"></td>
                            <td><?php echo stripslashes($osteopathieIndication->titre); ?></td>
                            <td><?php echo stripslashes($osteopathieIndication->texte); ?></td>
                            <td>?</td>
                            <td><a href="osteopathie/edit-indication/<?php echo $osteopathieIndication->id; ?>"><i class="fa fa-pencil-square" aria-hidden="true"></i></a></td>
                            <td><a href="osteopathie/delete-indication/<?php echo $osteopathieIndication->id; ?>"><i class="fa fa-minus-circle" aria-hidden="true"></i></a></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
            <!-- Fin Liste -->
            <h3>Ajouter une nouvelle indication :</h3>
            <!-- Formulaire -->
            <form action="./osteopathie/add-indication" method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="titre">Titre <span>*</span> :</label>
                    <input type="text" name="titre" id="titre" class="form-control">
                </div>
                <div class="form-group">
                    <label for="texte">Texte <span>*</span> :</label>
                    <input type="text" name="texte" id="texte" class="form-control">
                </div>
                <div class="form-group">
                    <label for="image">Image <span>*</span> :</label>
                    <input type="file" name="image" id="image">
                </div>
                <div class="form-group">
                    <label for="oIndActive">Active :</label>
                    <input type="checkbox" name="oIndActive" id="oIndActive">
                </div>
                <div class="text-center">
                    <button type="submit" class="btn btn-warning" id="bandeau">Ajouter</button>
                </div>
            </form>
            <!-- Fin Formulaire -->
        </div>
        <!-- FIN BLOC INDICATION -->
        <hr>
    </div>
    <!-- FIN CONTAINER PRINCIPAL -->
    <!-- On gère le footer développé dans un autre fichier (CF admin-footer.inc.php) via un include. -->
    <?php include('view/inc/admin-footer.inc.php'); ?>
    <!-- On gère ici les scripts JS via l'include du fichier admin-js.inc.php -->
    <?php include('view/inc/admin-js.inc.php'); ?>
</body>

</html>