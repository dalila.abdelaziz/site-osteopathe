<!DOCTYPE html>
<html lang="fr">

<head>
    <?php
    include('view/inc/head-admin.inc.php');
    ?>
    <title>Admin - Conseils</title>
</head>

<body>
    <!-- HEADER -->
    <?php include('view/inc/header-admin.inc.php'); ?>
    <!-- Gérer la taille du texte affichée -->
    <?php
    // Réduire la taille des texte à 50 caractères
    function tailleAffichage($texte)
    {
        if (strlen($texte) >= 50) {
            $texte  = trim(substr($texte, 0, 50));
            $texte .= '...';
        }
        return $texte;
    }
    ?>
    <!-- BANDEAU -->
    <div class="container-fluid entete">
        <div class="container text-center bandeau">
            <h2>Admin-Conseils</h2>
            <hr class="hr-bandeau">
            <p>Bienvenue sur votre backoffice.</p>
            <p>D'ici, vous pouvez gérer le contenu de votre page conseils.</p>
        </div>
    </div>
    <div class="container conseil-contenu">
        <a href="<?php echo BASE_FOLDER; ?>/admin/dashboard" class="btn bg-grey text-white my-3">Retour au dashboard</a>
        <!-- Administrer le bandeau -->
        <div class="p-4 my-3 conseil-bandeau shadow">
            <h3>Administrer le bandeau de la page Conseils</h3>
            <!-- Liste des titres du bandeau -->
            <h4>&bull; <u>Liste des titres du bandeau</u></h4>
            <table class="table table-borderless text-white">
                <thead class="text-center bg-orange">
                    <tr>
                        <th scope="col">Titre</th>
                        <th scope="col">Soustitre</th>
                        <th scope="col">Paratexte</th>
                        <th scope="col">Modification</th>
                        <th scope="col">Suppression</th>
                    </tr>
                </thead>
                <tbody class="text-center">
                    <?php foreach ($conseilBandeaux as $conseilBandeau) { ?>
                        <tr>
                            <td><?php echo $conseilBandeau->titre; ?></td>
                            <td><?php echo $conseilBandeau->soustitre; ?></td>
                            <td><?php echo $conseilBandeau->paratexte; ?></td>
                            <td><a href="conseils/edit-bandeau/<?php echo $conseilBandeau->id_bandeau; ?>" class="orange-perso lien-icon"><i class="fa fa-pencil-square" aria-hidden="true"></i></a></td>
                            <td><a href="conseils/delete-bandeau/<?php echo $conseilBandeau->id_bandeau; ?>" class="orange-perso lien-icon"><i class="fa fa-window-close" aria-hidden="true"></i></a></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
            <!-- Formulaire d'ajout -->
            <h4>&bull; <u>Ajouter un bandeau</u></h4>
            <form action="./conseils/add-bandeau" method="post" class="text-white">
                <div class="form-group">
                    <label for="titre">Titre:</label>
                    <input type="text" name="titre" id="titre" class="form-control" placeholder="Votre titre">
                </div>
                <div class="form-group">
                    <label for="soustitre">Soustitre:</label>
                    <input type="text" name="soustitre" id="soustitre" class="form-control" placeholder="Votre soustitre">
                </div>
                <div class="form-group">
                    <label for="paratexte">Paratexte:</label>
                    <input type="text" name="paratexte" id="paratexte" class="form-control" placeholder="Votre paratexte">
                </div>
                <div class="text-right">
                    <button type="submit" class="btn bg-orange text-white">Ajouter un bandeau</button>
                </div>
            </form>
        </div>
        <hr class="hr-conseil">
        <!-- Administrer les articles conseils -->
        <div class="p-4 my-3 conseil-article shadow">
            <h3>Administrer les articles de la page Conseils</h3>
            <!-- Liste des conseils -->
            <h4>&bull; <u>Liste des articles</u></h4>
            <table class="table table-borderless text-white">
                <thead class="bg-orange text-center">
                    <tr>
                        <th scope="col">Image</th>
                        <th scope="col">Titre</th>
                        <th scope="col">Texte</th>
                        <th scope="col">Modification</th>
                        <th scope="col">Suppression</th>
                    </tr>
                </thead>
                <tbody class="text-center">
                    <?php foreach ($conseilArticles as $conseilArticle) { ?>
                        <tr>
                            <td><img src="<?php echo BASE_FOLDER; ?>/assets/images/<?php echo $conseilArticle->image; ?>" alt="<?php echo $conseilArticle->titre_article; ?>" class="img-conseil"></td>
                            <td><?php echo $conseilArticle->titre_article; ?></td>
                            <td><?php echo tailleAffichage($conseilArticle->texte); ?></td>
                            <td><a href="conseils/edit-article/<?php echo $conseilArticle->id_article; ?>" class="orange-perso lien-icon"><i class="fa fa-pencil-square" aria-hidden="true"></i></a></td>
                            <td><a href="conseils/delete-article/<?php echo $conseilArticle->id_article; ?>" class="orange-perso lien-icon"><i class="fa fa-window-close" aria-hidden="true"></i></a></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
            <!-- Formulaire d'ajout -->
            <h4>&bull; <u>Ajouter un article</u></h4>
            <form action="./conseils/add-article" method="post" enctype="multipart/form-data" class="text-white">
                <input type="hidden" name="createdAt" id="todayDate" />
                <div class="form-group">
                    <label for="titre_article">Titre de l'article:</label>
                    <input type="text" name="titre_article" id="titre_article" class="form-control" placeholder="Entrez le titre de l'article">
                </div>
                <div class="form-group">
                    <label for="texte">Contenu de l'article:</label>
                    <textarea name="texte" id="texte" rows="5" class="form-control" placeholder="Votre article"></textarea>
                </div>
                <div class="form-group">
                    <label for="image">Image:</label>
                    <input type="file" name="image" id="image" class="form-control">
                </div>
                <div class="text-right">
                    <button type="submit" class="btn bg-orange text-white">Ajouter un article</button>
                </div>
            </form>
        </div>
    </div>
    <!-- FOOTER -->
    <?php include('view/inc/footer-admin.inc.php'); ?>
    <!-- CDN JS -->
    <?php include('view/inc/js.inc.php'); ?>
    <script type="text/javascript">
        function getDate() {
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();
            if (dd < 10) {
                dd = '0' + dd
            }
            if (mm < 10) {
                mm = '0' + mm
            }
            today = yyyy + "" + mm + "" + dd;

            document.getElementById("todayDate").value = today;
        }

        //call getDate() when loading the page
        getDate();
    </script>
</body>

</html>