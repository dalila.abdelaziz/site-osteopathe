<!DOCTYPE html>
<html lang="fr">

<head>
    <?php include('view/inc/admin-head.inc.php'); ?>
    <title>Modifier Bandeau - Tarifs & Contacts</title>
</head>

<body>
    <?php include('view/inc/admin-header.inc.php'); ?>
    <div class="container py-3">
        <div class="p-4 my-3 shadow bg-dark">
            <h3>Modifier un bandeau de la page Tarifs & Contacts :</h3>
            <form action="../update-bandeau" method="post">
                <input type="hidden" name="id" value="<?php echo $tarifscontactsBandeau['id']; ?>">
                <div class="form-group">
                    <label for="titre">Titre <span>*</span> :</label>
                    <input type="text" name="titre" id="titre" class="form-control" value="<?php echo $tarifscontactsBandeau['titre']; ?>">
                </div>
                <div class="form-group">
                    <label for="soustitre">Sous-titre <span>*</span> :</label>
                    <input type="text" name="soustitre" id="soustitre" class="form-control" value="<?php echo $tarifscontactsBandeau['soustitre']; ?>">
                </div>
                <div class="form-group">
                    <label for="paratexte">Para-texte <span>*</span> :</label>
                    <input type="text" name="paratexte" id="paratexte" class="form-control" value="<?php echo $tarifscontactsBandeau['paratexte']; ?>">
                </div>
                <div class="text-center">
                    <button type="submit" class="btn btn-warning">Modifier</button>
                </div>
            </form>
        </div>
    </div>
    <?php include('view/inc/admin-footer.inc.php'); ?>
    <?php include('view/inc/admin-js.inc.php'); ?>
</body>

</html>