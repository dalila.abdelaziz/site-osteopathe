<!DOCTYPE html>
<html lang="fr">

<head>
    <?php
    include('view/inc/head-admin.inc.php');
    ?>
    <title>Admin - Modifier - Contact Accueil</title>
</head>

<body>
    <!-- HEADER -->
    <?php include('view/inc/header-admin.inc.php'); ?>
    <!-- BANDEAU -->
    <div class="container-fluid entete">
        <div class="container text-center bandeau">
            <h2>Admin-Accueil</h2>
            <hr class="hr-bandeau">
            <p>Bienvenue sur votre backoffice.</p>
            <p>D'ici, vous pouvez gérer le contenu de votre page Accueil.</p>
        </div>
    </div>
        <!-- FORMULAIRE DE MODIFICATION -->
        <div class="container home-contenu home-contact p-3 my-3 shadow">
        <a href="<?php echo BASE_FOLDER; ?>/admin/accueil" class="btn bg-orange text-white mb-3">Retour à la page admin-accueil</a>
        <h3>Modifier mes contacts</h3>
            <form action="../update-contact-home" method="post" class="text-white">
            <input type="hidden" name="id" value="<?php echo $home_contact['id_contact']; ?>">
                <div class="form-group">
                    <label for="adresse">Adresse:</label>
                    <input type="text" name="adresse" id="adresse" class="form-control" value="<?php echo stripslashes($home_contact['adresse']); ?>">
                </div>
                <div class="form-group">
                    <label for="telephone">Téléphone:</label>
                    <input type="text" name="telephone" id="telephone" class="form-control" value="<?php echo stripslashes($home_contact['telephone']); ?>">
                </div>
                <div class="form-group">
                    <label for="email">Email:</label>
                    <input type="text" name="email" id="email" class="form-control" value="<?php echo stripslashes($home_contact['email']); ?>">
                </div>
                <div class="text-right">
                    <button type="submit" class="btn bg-orange text-white">Modifier</button>
                </div>
            </form>
        </div>
    <!-- CDN JS -->
    <?php include('view/inc/js.inc.php'); ?>
</body>

</html>