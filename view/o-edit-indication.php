<!DOCTYPE html>
<html lang="fr">

<head>
    <?php include('view/inc/admin-head.inc.php'); ?>
    <title>Modifier Indication - Ostéopathie</title>
</head>

<body>
    <?php include('view/inc/admin-header.inc.php'); ?>
    <div class="container py-3">
        <div class="p-4 my-3 shadow bg-dark">
            <h3>Modifier une indication de la page Ostéopathie :</h3>
            <form action="../update-indication" method="post" enctype="multipart/form-data">
                <input type="hidden" name="id" value="<?php echo $osteopathieIndication['id']; ?>">
                <input type="hidden" name="img-name" value="<?php echo $osteopathieIndication['image']?>">
                <div class="col-2">
                    <img src="<?php echo BASE_FOLDER.'/assets/images/'.$osteopathieIndication['image'];?>" alt="image indication" class="img-fluid">
                </div>
                <div class="form-group">
                    <label for="image">Image :</label>
                    <input type="file" name="image" id="image">
                </div>
                <div class="form-group">
                    <label for="titre">Titre :</label>
                    <input type="text" name="titre" id="titre" class="form-control" value="<?php echo stripslashes($osteopathieIndication['titre']); ?>">
                </div>
                <div class="form-group">
                    <label for="texte">Texte :</label>
                    <input type="text" name="texte" id="texte" class="form-control" value="<?php echo stripslashes($osteopathieIndication['texte']); ?>">
                </div>
                <div class="text-center">
                    <button type="submit" class="btn btn-warning">Modifier</button>
                </div>
            </form>
        </div>
    </div>
    <?php include('view/inc/admin-footer.inc.php'); ?>
    <?php include('view/inc/admin-js.inc.php'); ?>
</body>

</html>