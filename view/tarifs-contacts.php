<?php
// Si la session n'est pas déjà lancée, on la démarre.
if (!isset($_SESSION)) session_start();
?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <!-- On gère ici les méta données, les CDN's et les links CSS via l'include du fichier admin-head.inc.php -->
    <?php include('view/inc/admin-head.inc.php'); ?>
    <title>Admin Gestion Tarifs & Contact</title>
</head>

<body class="bg-light">
    <!-- HEADER -->
    <!-- On gère le header développé dans un autre fichier (CF admin-header.inc.php) via un include. -->
    <?php include('view/inc/admin-header.inc.php'); ?>
    <!-- FIN HEADER -->
    <!-- BANDEAU BACK TARIFS & CONTACTS -->
    <div class="container-fluid bandeau">
        <h1 class="my-5">Espace Administrateur</h1>
        <hr>
        <h2 class="my-5">Tarifs - Contact - Infos Complémentaires</h2>
        <hr>
        <h5 class="my-5">Bienvenue sur votre backoffice. D'ici, vous pouvez gérer le contenu de l'intégralité de votre page Tarifs & Contact.</h5>
    </div>
    <!-- FIN BANDEAU BACK TARIFS & CONTACTS -->
    <!-- CONTAINER PRINCIPAL englobant le reste du contenu attendu sur cette page : listes et formulaires d'ajout -->
    <div class="container-fluid py-3">
        <!-- BLOC BANDEAU -->
        <div class="p-4 my-3 shadow bg-dark" id="bandeau">
            <?php
            if (isset($_SESSION['message-bandeau'])) {
                echo '<div class="alert alert-success">' . $_SESSION['message-bandeau'] . '</div>';
                unset($_SESSION['message-bandeau']);
            }
            ?>
            <!-- Liste -->
            <h3>Liste des bandeaux :</h3>
            <!-- On crée ici une table nous permettant l'affichage statique (noms des champs) et dynamique (contenus renseignés) des données validées dans un formulaire -->
            <table class="table table-borderless">
                <!-- thead correspond à la partie de la table attendant les données d'en tête -->
                <thead class="text-center">
                    <!-- tr correspond à la création d'une ligne attendant du contenu -->
                    <tr>
                        <!-- th correspond aux contenus renseignés dans la ligne de l'en-tête de la table (soit les intitulés des champs à renseigner) -->
                        <th scope="col">Titre</th>
                        <th scope="col">Sous-Titre</th>
                        <th scope="col">Paratexte</th>
                        <th scope="col">Active</th>
                        <th scope="col">Modifier</th>
                        <th scope="col">Supprimer</th>
                    </tr>
                </thead>
                <!-- tbody correspond à la partie de la table attendant les données renseignées lors de l'ajout du formulaire (contenus des champs) -->
                <tbody class="text-center">
                    <?php foreach ($tarifscontactsBandeaux as $tarifscontactsBandeau) { ?>
                        <tr>
                            <!-- td correspond ici aux datas rensiegnées pour chaque champ -->
                            <td><?php echo $tarifscontactsBandeau->titre; ?></td>
                            <td><?php echo $tarifscontactsBandeau->soustitre; ?></td>
                            <td><?php echo $tarifscontactsBandeau->paratexte; ?></td>
                            <td>?</td>
                            <td><a href="tarifs-contacts/edit-bandeau/<?php echo $tarifscontactsBandeau->id; ?>"><i class="fa fa-pencil-square" aria-hidden="true"></i></a></td>
                            <td><a href="tarifs-contacts/delete-bandeau/<?php echo $tarifscontactsBandeau->id; ?>"><i class="fa fa-minus-circle" aria-hidden="true"></i></a></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
            <!-- Fin Liste -->
            <h3>Ajouter un nouveau bandeau :</h3>
            <!-- Formulaire -->
            <form action="./tarifs-contacts/add-bandeau" method="post">
                <div class="form-group">
                    <label for="titre">Titre <span>*</span> :</label>
                    <input type="text" name="titre" id="titre" class="form-control">
                </div>
                <div class="form-group">
                    <label for="soustitre">Sous-titre <span>*</span> :</label>
                    <input type="text" name="soustitre" id="soustitre" class="form-control">
                </div>
                <div class="form-group">
                    <label for="paratexte">Para-texte <span>*</span> :</label>
                    <input type="text" name="paratexte" id="paratexte" class="form-control">
                </div>
                <div class="form-group">
                    <label for="banActive">Active :</label>
                    <input type="checkbox" name="banActive" id="banActive">
                </div>
                <div class="text-center">
                    <button type="submit" class="btn btn-warning" id="bandeau">Ajouter</button>
                </div>
            </form>
            <!-- Fin Formulaire -->
        </div>
        <!-- FIN BLOC BANDEAU -->
        <hr>
        <!-- BLOC TARIFS & PRESTA -->
        <div class="p-4 my-3 shadow bg-dark">
            <?php
            if (isset($_SESSION['message-tarif'])) {
                echo '<div class="alert alert-success">' . $_SESSION['message-tarif'] . '</div>';
                unset($_SESSION['message-tarif']);
            } ?>
            <!-- Liste -->
            <h3>Liste des prestations et tarifs :</h3>
            <!-- On crée ici une table nous permettant l'affichage statique (noms des champs) et dynamique (contenus renseignés) des données validées dans un formulaire -->
            <table class="table table-borderless">
                <!-- thead correspond à la partie de la table attendant les données d'en tête -->
                <thead class="text-center table-borderless">
                    <!-- tr correspond à la création d'une ligne attendant du contenu -->
                    <tr>
                        <!-- th correspond aux contenus renseignés dans la ligne de l'en-tête de la table (soit les intitulés des champs à renseigner) -->
                        <th scope="col">Prestation</th>
                        <th scope="col">Tarif</th>
                        <th scope="col">Active</th>
                        <th scope="col">Modifier</th>
                        <th scope="col">Supprimer</th>
                    </tr>
                </thead>
                <!-- tbody correspond à la partie de la table attendant les données renseignées lors de l'ajout du formulaire (contenus des champs) -->
                <tbody class="text-center">
                    <?php foreach ($tarifscontactsTarifs as $tarifscontactsTarif) { ?>
                        <tr>
                            <!-- td correspond ici aux datas rensiegnées pour chaque champ -->
                            <td><?php echo $tarifscontactsTarif->presta; ?></td>
                            <td><?php echo $tarifscontactsTarif->tarif; ?></td>
                            <td>?</td>
                            <td><a href="tarifs-contacts/edit-tarif/<?php echo $tarifscontactsTarif->id; ?>"><i class="fa fa-pencil-square" aria-hidden="true"></i></a></td>
                            <td><a href="tarifs-contacts/delete-tarif/<?php echo $tarifscontactsTarif->id; ?>"><i class="fa fa-minus-circle" aria-hidden="true"></i></a></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
            <!-- Fin Liste -->
            <h3>Ajouter des prestations et tarifs :</h3>
            <!-- FORMULAIRE PRESTATIONS ET TARIFS -->
            <!-- A voir si dessous dans l'action du form, on insère le ./add ou autre indication d'action lors de la soumission du form -->
            <form action="./tarifs-contacts/add-tarif" method="post">
                <div class="form-group">
                    <label for="prestation">Prestation <span>*</span> :</label>
                    <input type="text" name="presta" id="prestation" class="form-control">
                </div>
                <div class="form-group">
                    <label for="tarif">Tarif <span>*</span> :</label>
                    <input type="text" name="tarif" id="tarif" class="form-control">
                </div>
                <div class="form-group">
                    <label for="tarActive">Active :</label>
                    <input type="checkbox" name="tarActive" id="tarActive">
                </div>
                <div class="text-center">
                    <button type="submit" class="btn btn-warning">Ajouter</button>
                </div>
            </form>
            <!-- FIN FORMULAIRE PRESTATIONS ET TARIFS -->
        </div>
        <!-- FIN BLOC TARIFS & PRESTA -->
        <hr>
        <!-- BLOC CONTACTS -->
        <div class="p-4 my-3 shadow bg-dark">
            <!-- Liste -->
            <h3>Liste des contacts :</h3>
            <?php
            if (isset($_SESSION['message-contact'])) {
                echo '<div class="alert alert-success">' . $_SESSION['message-contact'] . '</div>';
                unset($_SESSION['message-contact']);
            }
            ?>
            <!-- On crée ici une table nous permettant l'affichage statique (noms des champs) et dynamique (contenus renseignés) des données validées dans un formulaire -->
            <table class="table table-borderless">
                <!-- thead correspond à la partie de la table attendant les données d'en tête -->
                <thead class="text-center">
                    <!-- tr correspond à la création d'une ligne attendant du contenu -->
                    <tr>
                        <!-- th correspond aux contenus renseignés dans la ligne de l'en-tête de la table (soit les intitulés des champs à renseigner) -->
                        <th scope="col">Adresse</th>
                        <th scope="col">CP</th>
                        <th scope="col">Ville</th>
                        <th scope="col">Tel</th>
                        <th scope="col">Mail</th>
                        <th scope="col">T1</th>
                        <th scope="col">T2</th>
                        <th scope="col">T3</th>
                        <th scope="col">T4</th>
                        <th scope="col">Active</th>
                        <th scope="col">Modifier</th>
                        <th scope="col">Supprimer</th>
                    </tr>
                </thead>
                <!-- tbody correspond à la partie de la table attendant les données renseignées lors de l'ajout du formulaire (contenus des champs) -->
                <tbody class="text-center">
                    <?php foreach ($tarifscontactsContacts as $tarifscontactsContact) { ?>
                        <tr>
                            <!-- td correspond ici aux datas rensiegnées pour chaque champ -->
                            <td><?php echo $tarifscontactsContact->adresse; ?></td>
                            <td><?php echo $tarifscontactsContact->zipcode; ?></td>
                            <td><?php echo $tarifscontactsContact->ville; ?></td>
                            <td><?php echo $tarifscontactsContact->tel; ?></td>
                            <td><?php echo $tarifscontactsContact->mail; ?></td>
                            <td><?php echo $tarifscontactsContact->t1; ?></td>
                            <td><?php echo $tarifscontactsContact->t2; ?></td>
                            <td><?php echo $tarifscontactsContact->t3; ?></td>
                            <td><?php echo $tarifscontactsContact->t4; ?></td>
                            <td>?</td>
                            <td><a href="tarifs-contacts/edit-contact/<?php echo $tarifscontactsContact->id; ?>"><i class="fa fa-pencil-square" aria-hidden="true"></i></a></td>
                            <td><a href="tarifs-contacts/delete-contact/<?php echo $tarifscontactsContact->id; ?>"><i class="fa fa-minus-circle" aria-hidden="true"></i></a></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
            <!-- Fin Liste -->
            <h3>Ajouter des contacts :</h3>
            <!-- FORMULAIRE CONTACTS -->
            <!-- A voir si dessous dans l'action du form, on insère le ./add ou autre indication d'action lors de la soumission du form -->
            <form action="./tarifs-contacts/add-contact" method="post">
                <div class="form-group">
                    <label for="adresse">Adresse <span>*</span> :</label>
                    <input type="text" name="adresse" id="adresse" class="form-control" required>
                </div>
                <div class="form-group">
                    <label for="zipcode">Code Postal <span>*</span> :</label>
                    <input type="text" name="zipcode" id="zipcode" class="form-control" required>
                </div>
                <div class="form-group">
                    <label for="ville">Ville <span>*</span> :</label>
                    <input type="text" name="ville" id="ville" class="form-control" required>
                </div>
                <div class="form-group">
                    <label for="tel">Téléphone <span>*</span> :</label>
                    <input type="text" name="tel" id="tel" class="form-control" required>
                </div>
                <div class="form-group">
                    <label for="mail">Email <span>*</span> :</label>
                    <input type="text" name="mail" id="mail" class="form-control" required>
                </div>
                <div class="form-group">
                    <label for="t1">Moyen de transport 1 <span>*</span> :</label>
                    <input type="text" name="t1" id="t1" class="form-control" required>
                </div>
                <div class="form-group">
                    <label for="t2">Moyen de transport 2 :</label>
                    <input type="text" name="t2" id="t2" class="form-control">
                </div>
                <div class="form-group">
                    <label for="t3">Moyen de transport 3 :</label>
                    <input type="text" name="t3" id="t3" class="form-control">
                </div>
                <div class="form-group">
                    <label for="t4">Moyen de transport 4 :</label>
                    <input type="text" name="t4" id="t4" class="form-control">
                </div>
                <div class="form-group">
                    <label for="conActive">Active :</label>
                    <input type="checkbox" name="conActive" id="conActive">
                </div>
                <div class="text-center">
                    <button type="submit" class="btn btn-warning">Ajouter</button>
                </div>
            </form>
            <!-- FIN FORMULAIRE CONTACTS -->
        </div>
        <!-- FIN BLOC CONTACTS -->
        <hr>
        <!-- BLOC INFOS COMP -->
        <div class="p-4 my-3 shadow bg-dark">
            <!-- Liste -->
            <h3>Liste des informations complémentaires :</h3>
            <?php
            if (isset($_SESSION['message-infocomp'])) {
                echo '<div class="alert alert-success">' . $_SESSION['message-infocomp'] . '</div>';
                unset($_SESSION['message-infocomp']);
            }
            ?>
            <!-- On crée ici une table nous permettant l'affichage statique (noms des champs) et dynamique (contenus renseignés) des données validées dans un formulaire -->
            <table class="table table-borderless">
                <!-- thead correspond à la partie de la table attendant les données d'en tête -->
                <thead class="text-center">
                    <!-- tr correspond à la création d'une ligne attendant du contenu -->
                    <tr>
                        <!-- th correspond aux contenus renseignés dans la ligne de l'en-tête de la table (soit les intitulés des champs à renseigner) -->
                        <th scope="col">Info 1</th>
                        <th scope="col">Info 2</th>
                        <th scope="col">Info 3</th>
                        <th scope="col">Info 4</th>
                        <th scope="col">Info 5</th>
                        <th scope="col">Active</th>
                        <th scope="col">Modifier</th>
                        <th scope="col">Supprimer</th>
                    </tr>
                </thead>
                <!-- tbody correspond à la partie de la table attendant les données renseignées lors de l'ajout du formulaire (contenus des champs) -->
                <tbody class="text-center">
                    <?php foreach ($tarifscontactsInfosComps as $tarifscontactsInfoComp) { ?>
                        <tr>
                            <!-- td correspond ici aux datas rensiegnées pour chaque champ -->
                            <td><?php echo stripslashes($tarifscontactsInfoComp->info1); ?></td>
                            <td><?php echo stripslashes($tarifscontactsInfoComp->info2); ?></td>
                            <td><?php echo stripslashes($tarifscontactsInfoComp->info3); ?></td>
                            <td><?php echo stripslashes($tarifscontactsInfoComp->info4); ?></td>
                            <td><?php echo stripslashes($tarifscontactsInfoComp->info5); ?></td>
                            <td>?</td>
                            <td><a href="tarifs-contacts/edit-infocomp/<?php echo $tarifscontactsInfoComp->id; ?>"><i class="fa fa-pencil-square" aria-hidden="true"></i></a></td>
                            <td><a href="tarifs-contacts/delete-infocomp/<?php echo $tarifscontactsInfoComp->id; ?>"><i class="fa fa-minus-circle" aria-hidden="true"></i></a></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
            <!-- Fin Liste -->
            <h3>Ajouter des informations complémentaires :</h3>
            <!-- FORMULAIRE INFOS COMPLEMENTAIRES -->
            <!-- A voir si dessous dans l'action du form, on insère le ./add ou autre indication d'action lors de la soumission du form -->
            <form action="./tarifs-contacts/add-infocomp" method="post">
                <div class="form-group">
                    <label for="info1">Information 1 :</label>
                    <input type="text" name="info1" id="info1" class="form-control">
                </div>
                <div class="form-group">
                    <label for="info2">Information 2 :</label>
                    <input type="text" name="info2" id="info2" class="form-control">
                </div>
                <div class="form-group">
                    <label for="info3">Information 3 :</label>
                    <input type="text" name="info3" id="info3" class="form-control">
                </div>
                <div class="form-group">
                    <label for="info4">Information 4 :</label>
                    <input type="text" name="info4" id="info4" class="form-control">
                </div>
                <div class="form-group">
                    <label for="info5">Information 5 :</label>
                    <input type="text" name="info5" id="info5" class="form-control">
                </div>
                <div class="form-group">
                    <label for="infActive">Active :</label>
                    <input type="checkbox" name="infActive" id="infActive">
                </div>
                <div class="text-center">
                    <button type="submit" class="btn btn-warning">Ajouter</button>
                </div>
            </form>
            <!-- FIN FORMULAIRE INFOS COMPLEMENTAIRES -->
        </div>
        <!-- FIN BLOC INFOS COMP -->
    </div>
    <!-- FIN CONTAINER PRINCIPAL -->
    <!-- On gère le footer développé dans un autre fichier (CF admin-footer.inc.php) via un include. -->
    <?php include('view/inc/admin-footer.inc.php'); ?>
    <!-- On gère ici les scripts JS via l'include du fichier admin-js.inc.php -->
    <?php include('view/inc/admin-js.inc.php'); ?>
</body>

</html>