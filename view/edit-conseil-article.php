<!DOCTYPE html>
<html lang="fr">

<head>
    <?php
    include('view/inc/head-admin.inc.php');
    ?>
    <title>Admin-Modifier-Article</title>
</head>

<body>
    <!-- HEADER -->
    <?php include('view/inc/header-admin.inc.php'); ?>
    <!-- BANDEAU -->
    <div class="container-fluid entete">
        <div class="container text-center bandeau">
            <h2>Admin-Conseils</h2>
            <hr class="hr-bandeau">
            <p>Bienvenue sur votre backoffice.</p>
            <p>D'ici, vous pouvez gérer le contenu de votre page conseils.</p>
        </div>
    </div>
    <!-- FORMULAIRE DE MODIFICATION -->
    <div class="container conseil-contenu conseil-edit p-3 my-3 shadow">
        <a href="<?php echo BASE_FOLDER; ?>/admin/conseils" class="btn bg-orange text-white mb-3">Retour à la page admin-conseil</a>
        <h3>Modifier un article</h3>
        <form action="../update-article" method="post" enctype="multipart/form-data" class="text-white">
            <input type="hidden" name="id" value="<?php echo $conseilArticle['id_article']; ?>">
            <input type="hidden" name="img-name" value="<?php echo $conseilArticle['image']; ?>">
            <input type="hidden" name="createdAt" id="todayDate" />
            <div class="form-group">
                <label for="titre_article">Titre de l'article:</label>
                <input type="text" name="titre_article" id="titre_article" class="form-control" value="<?php echo stripslashes($conseilArticle['titre_article']); ?>">
            </div>
            <div class="form-group">
                <label for="texte">Contenu de l'article:</label>
                <textarea name="texte" id="texte" rows="5" class="form-control"><?php echo stripslashes($conseilArticle['texte']); ?></textarea>
            </div>
            <div class="col-2 px-0">
                <img src="<?php echo BASE_FOLDER; ?>/assets/images/<?php echo $conseilArticle['image']; ?>" alt="<?php echo $conseilArticle['titre_article']; ?>" class="img-fluid">
            </div>
            <div class="form-group">
                <label for="image">Image:</label>
                <input type="file" name="image" id="image" class="form-control">
            </div>
            <div class="text-right">
                <button type="submit" class="btn bg-orange text-white">Modifier</button>
            </div>
        </form>
    </div>
    <!-- CDN JS -->
    <?php include('view/inc/js.inc.php'); ?>
    <script type="text/javascript">
        function getDate() {
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();
            if (dd < 10) {
                dd = '0' + dd
            }
            if (mm < 10) {
                mm = '0' + mm
            }
            today = yyyy + "" + mm + "" + dd;

            document.getElementById("todayDate").value = today;
        }

        //call getDate() when loading the page
        getDate();
    </script>
</body>

</html>