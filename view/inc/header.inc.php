<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="#">LOGO</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav mx-auto">
            <li class="nav-item active">
                <a class="nav-link" href="#">Accueil <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Ostéopathie</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Tarifs & Contact</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Conseils</a>
            </li>
            <li class="nav-item">
                <a class="nav-link btn" href="https://www.doctolib.fr/osteopathe/toulouse/jonathan-borreil">Prendre RDV</a>
            </li>
        </ul>
    </div>
</nav>