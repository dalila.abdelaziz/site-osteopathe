<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- CDN FONT -->
<script src="https://use.fontawesome.com/a522664c5b.js"></script>
<!-- CDN BOOTSTRAP -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
<!-- CUSTOM CSS -->
<link rel="stylesheet" href="<?php echo SCRIPT_ROOT; ?>/assets/css/styles.css">