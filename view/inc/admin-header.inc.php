<header>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="/accueil">
            <img src="/assets/images/logo-j-osteo-jonathan-borreil.png" width="30" height="30" class="d-inline-block align-top" alt="#">
            jb Ostéopathe D.O.
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav ml-auto">
                <a class="nav-link mr-4" href="#">Dashboard <span class="sr-only">(current)</span></a>
                <a class="nav-link mx-4" href="#">Accueil</a>
                <a class="nav-link mx-4" href="#">Ostéopathie</a>
                <a class="nav-link mx-4 active" href="#">Tarifs & Contact</a>
                <a class="nav-link ml-4" href="#">Conseils</a>
            </div>
        </div>
    </nav>
</header>