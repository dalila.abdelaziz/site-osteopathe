<!DOCTYPE html>
<html lang="fr">

<head>
    <?php include('view/inc/admin-head.inc.php'); ?>
    <title>Modifier Contact - Tarifs & Contacts</title>
</head>

<body>
    <?php include('view/inc/admin-header.inc.php'); ?>
    <div class="container py-3">
        <div class="p-4 my-3 shadow bg-dark">
            <h3>Modifier un contact de la page Tarifs & Contacts :</h3>
            <form action="../update-contact" method="post">
                <input type="hidden" name="id" value="<?php echo $tarifscontactsContact['id']; ?>">
                <div class="form-group">
                    <label for="adresse">Adresse <span>*</span> :</label>
                    <input type="text" name="adresse" id="adresse" class="form-control" value="<?php echo $tarifscontactsContact['adresse']; ?>">
                </div>
                <div class="form-group">
                    <label for="zipcode">Code Postal <span>*</span> :</label>
                    <input type="text" name="zipcode" id="zipcode" class="form-control" value="<?php echo $tarifscontactsContact['zipcode']; ?>">
                </div>
                <div class="form-group">
                    <label for="ville">Ville <span>*</span> :</label>
                    <input type="text" name="ville" id="ville" class="form-control" value="<?php echo $tarifscontactsContact['ville']; ?>">
                </div>
                <div class="form-group">
                    <label for="tel">Téléphone <span>*</span> :</label>
                    <input type="text" name="tel" id="tel" class="form-control" value="<?php echo $tarifscontactsContact['tel']; ?>">
                </div>
                <div class="form-group">
                    <label for="mail">Email <span>*</span> :</label>
                    <input type="text" name="mail" id="mail" class="form-control" value="<?php echo $tarifscontactsContact['mail']; ?>">
                </div>
                <div class="form-group">
                    <label for="t1">Transport 1 <span>*</span> :</label>
                    <input type="text" name="t1" id="t1" class="form-control" value="<?php echo $tarifscontactsContact['t1']; ?>">
                </div>
                <div class="form-group">
                    <label for="t2">Transport 2 <span>*</span> :</label>
                    <input type="text" name="t2" id="t2" class="form-control" value="<?php echo $tarifscontactsContact['t2']; ?>">
                </div>
                <div class="form-group">
                    <label for="t3">Transport 3 <span>*</span> :</label>
                    <input type="text" name="t3" id="t3" class="form-control" value="<?php echo $tarifscontactsContact['t3']; ?>">
                </div>
                <div class="form-group">
                    <label for="t4">Transport 4 <span>*</span> :</label>
                    <input type="text" name="t4" id="t4" class="form-control" value="<?php echo $tarifscontactsContact['t4']; ?>">
                </div>
                <div class="text-center">
                    <button type="submit" class="btn btn-warning">Modifier</button>
                </div>
            </form>
        </div>
    </div>
    <?php include('view/inc/admin-footer.inc.php'); ?>
    <?php include('view/inc/admin-js.inc.php'); ?>
</body>

</html>