<!DOCTYPE html>
<html lang="fr">

<head>
    <?php
    include('view/inc/head-admin.inc.php');
    ?>
    <title>Admin - Dashboard</title>
</head>

<body>
    <!-- HEADER -->
    <?php include('view/inc/header-admin.inc.php'); ?>
    <!-- BANDEAU -->
    <div class="container-fluid entete">
        <div class="container text-center bandeau">
            <h2>Admin-Dashboard</h2>
            <hr class="hr-bandeau">
            <p>Bienvenue sur votre backoffice.</p>
            <p>D'ici, vous pouvez gérer le contenu de toutes les pages de votre site web.</p>
        </div>
    </div>
    <!-- CARTES DES DIFFERENTES PAGES -->
    <div class="container dashboard text-center my-4">
        <div class="row justify-content-around">
            <div class="col-12 col-sm-5">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Accueil</h5>
                        <p class="card-text">Accédez au back-office de votre page Accueil. Ajoutez, modifiez ou supprimez du contenu comme vous le souhaitez !</p>
                        <a href="<?php echo BASE_FOLDER; ?>/admin/accueil" class="btn bg-orange text-white">Accéder à la page</a>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-5">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Ostéopathie</h5>
                        <p class="card-text">Accédez au back-office de votre page Ostéopathie. Ajoutez, modifiez ou supprimez du contenu comme vous le souhaitez !</p>
                        <a href="<?php echo BASE_FOLDER; ?>/admin/osteopathie" class="btn bg-orange text-white">Accéder à la page</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-4 justify-content-around">
            <div class="col-12 col-sm-5">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Tarifs & Contacts</h5>
                        <p class="card-text">Accédez au back-office de votre page Tarifs & Contact. Ajoutez, modifiez ou supprimez du contenu comme vous le souhaitez !</p>
                        <a href="<?php echo BASE_FOLDER; ?>/admin/tarifs-contact" class="btn bg-orange text-white">Accéder à la page</a>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-5">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Conseils</h5>
                        <p class="card-text">Accédez au back-office de votre page Conseils. Ajoutez, modifiez ou supprimez du contenu comme vous le souhaitez !</p>
                        <a href="<?php echo BASE_FOLDER; ?>/admin/conseils" class="btn bg-orange text-white">Accéder à la page</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- FOOTER -->
    <?php include('view/inc/footer-admin.inc.php'); ?>
    <!-- CDN JS -->
    <?php include('view/inc/js.inc.php'); ?>
    <script>
    $(window).on('load', function () {
        var h = 0;
        $('.card-body').each(function () {
            if ($(this).height() > h) h = $(this).height();
          // console.log("height", $(this).height());
        });
        //console.log("h=>",h);
        $('.card-body').height(h);
    })

</script>
</body>

</html>