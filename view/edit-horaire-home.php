<!DOCTYPE html>
<html lang="fr">

<head>
    <?php
    include('view/inc/head-admin.inc.php');
    ?>
    <title>Admin - Modifier - Horaires Accueil</title>
</head>

<body>
    <!-- HEADER -->
    <?php include('view/inc/header-admin.inc.php'); ?>
    <!-- BANDEAU -->
    <div class="container-fluid entete">
        <div class="container text-center bandeau">
            <h2>Admin-Accueil</h2>
            <hr class="hr-bandeau">
            <p>Bienvenue sur votre backoffice.</p>
            <p>D'ici, vous pouvez gérer le contenu de votre page Accueil.</p>
        </div>
    </div>
        <!-- FORMULAIRE DE MODIFICATION -->
        <div class="container home-contenu home-horaire p-3 my-3 shadow">
        <a href="<?php echo BASE_FOLDER; ?>/admin/accueil" class="btn bg-orange text-white mb-3">Retour à la page admin-accueil</a>
        <h3>Modifier mes horaires</h3>
            <form action="../update-horaire-home" method="post" class="text-white">
            <input type="hidden" name="id" value="<?php echo $home_horaire['id_horaire']; ?>">
                <div class="form-group">
                    <label for="jour_ouverture">Jour d'ouverture:</label>
                    <input type="text" name="jour_ouverture" id="jour_ouverture" class="form-control" value="<?php echo stripslashes($home_horaire['jour_ouverture']); ?>">
                </div>
                <div class="form-group">
                    <label for="heure_ouverture">Heure d'ouverture:</label>
                    <input type="text" name="heure_ouverture" id="heure_ouverture" class="form-control" value="<?php echo stripslashes($home_horaire['heure_ouverture']); ?>">
                </div>
                <div class="form-group">
                    <label for="heure_fermeture">Heure de fermeture:</label>
                    <input type="text" name="heure_fermeture" id="heure_fermeture" class="form-control" value="<?php echo stripslashes($home_horaire['heure_fermeture']); ?>">
                </div>
                <div class="text-right">
                    <button type="submit" class="btn bg-orange text-white">Modifier</button>
                </div>
            </form>
        </div>
    <!-- CDN JS -->
    <?php include('view/inc/js.inc.php'); ?>
</body>

</html>