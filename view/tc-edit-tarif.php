<!DOCTYPE html>
<html lang="fr">

<head>
    <?php include('view/inc/admin-head.inc.php'); ?>
    <title>Modifier Tarif - Tarifs & Contacts</title>
</head>

<body>
    <?php include('view/inc/admin-header.inc.php'); ?>
    <div class="container py-3">
        <div class="p-4 my-3 shadow bg-dark">
            <h3>Modifier un duo presta/tarif de la page Tarifs & Contacts :</h3>
            <form action="../update-tarif" method="post">
                <input type="hidden" name="id" value="<?php echo $tarifscontactsTarif['id']; ?>">
                <div class="form-group">
                    <label for="presta">Prestation <span>*</span> :</label>
                    <input type="text" name="presta" id="presta" class="form-control" value="<?php echo $tarifscontactsTarif['presta']; ?>">
                </div>
                <div class="form-group">
                    <label for="tarif">Tarif <span>*</span> :</label>
                    <input type="text" name="tarif" id="tarif" class="form-control" value="<?php echo $tarifscontactsTarif['tarif']; ?>">
                </div>
                <div class="text-center">
                    <button type="submit" class="btn btn-warning">Modifier</button>
                </div>
            </form>
        </div>
    </div>
    <?php include('view/inc/admin-footer.inc.php'); ?>
    <?php include('view/inc/admin-js.inc.php'); ?>
</body>

</html>