<!DOCTYPE html>
<html lang="fr">

<head>
    <?php
    include('view/inc/head-admin.inc.php');
    ?>
    <title>Admin - Modifier Bandeau Conseils</title>
</head>

<body>
    <!-- HEADER -->
    <?php include('view/inc/header-admin.inc.php'); ?>
    <!-- BANDEAU -->
    <div class="container-fluid entete">
        <div class="container text-center bandeau">
            <h2>Admin-Conseils</h2>
            <hr class="hr-bandeau">
            <p>Bienvenue sur votre backoffice.</p>
            <p>D'ici, vous pouvez gérer le contenu de votre page conseils.</p>
        </div>
    </div>
    <!-- FORMULAIRE DE MODIFICATION -->
    <div class="container conseil-contenu conseil-edit p-3 my-3 shadow">
        <a href="<?php echo BASE_FOLDER; ?>/admin/conseils" class="btn bg-orange text-white mb-3">Retour à la page admin-conseil</a>
        <h3>Modifier le contenu du bandeau</h3>
        <form action="../update-bandeau" method="post" class="text-white">
            <input type="hidden" name="id" value="<?php echo $conseilBandeau['id_bandeau']; ?>">
            <div class="form-group">
                <label for="titre">Titre:</label>
                <input type="text" name="titre" id="titre" class="form-control" value="<?php echo stripslashes($conseilBandeau['titre']); ?>">
            </div>
            <div class="form-group">
                <label for="soustitre">Soustitre:</label>
                <input name="soustitre" id="soustitre" class="form-control" value="<?php echo stripslashes($conseilBandeau['soustitre']); ?>">
            </div>
            <div class="form-group">
                <label for="paratexte">Paratexte:</label>
                <input name="paratexte" id="paratexte" class="form-control" value="<?php echo stripslashes($conseilBandeau['paratexte']); ?>">
            </div>
            <div class="text-right">
                <button type="submit" class="btn bg-orange text-white">Modifier</button>
            </div>
        </form>
    </div>
    <!-- CDN JS -->
    <?php include('view/inc/js.inc.php'); ?>
</body>

</html>