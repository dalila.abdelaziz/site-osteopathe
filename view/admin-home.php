<!DOCTYPE html>
<html lang="fr">

<head>
    <?php
    include('view/inc/head-admin.inc.php');
    ?>
    <title>Admin - Accueil</title>
</head>

<body>
    <!-- HEADER -->
    <?php include('view/inc/header-admin.inc.php'); ?>
    <!-- Gérer la taille du texte affichée -->
    <?php
    // Réduire la taille des texte à 50 caractères
    function tailleAffichage($texte)
    {
        if (strlen($texte) >= 50) {
            $texte  = trim(substr($texte, 0, 50));
            $texte .= '...';
        }
        return $texte;
    }
    ?>
    <!-- BANDEAU -->
    <div class="container-fluid entete">
        <div class="container text-center bandeau">
            <h2>Admin-Accueil</h2>
            <hr class="hr-bandeau">
            <p>Bienvenue sur votre backoffice.</p>
            <p>D'ici, vous pouvez gérer le contenu de votre page Accueil.</p>
        </div>
    </div>
    <div class="container home-contenu">
    <a href="<?php echo BASE_FOLDER; ?>/admin/dashboard" class="btn bg-grey text-white my-3">Retour au dashboard</a>
        <!-- Administrer le bandeau -->
        <div class="p-4 my-3 home-bandeau shadow">
            <h3>Administrer le bandeau de la page Accueil</h3>
            <!-- Liste des titres du bandeau -->
            <h4>&bull; <u>Liste des titres du bandeau</u></h4>
            <table class="table table-borderless text-white">
                <thead class="text-center bg-orange text-white">
                    <tr>
                        <th scope="col">Titre 1</th>
                        <th scope="col">Titre 2</th>
                        <th scope="col">Titre 3</th>
                        <th scope="col">Titre 4</th>
                        <th scope="col">Texte</th>
                        <th scope="col">Modification</th>
                        <th scope="col">Suppression</th>
                    </tr>
                </thead>
                <tbody class="text-center">
                    <?php foreach ($home_bandeaux as $home_bandeau) { ?>
                        <tr>
                            <td><?php echo $home_bandeau->titre_1; ?></td>
                            <td><?php echo $home_bandeau->titre_2; ?></td>
                            <td><?php echo $home_bandeau->titre_3; ?></td>
                            <td><?php echo $home_bandeau->titre_4; ?></td>
                            <td><?php echo tailleAffichage($home_bandeau->texte_bandeau); ?></td>
                            <td><a href="accueil/edit-bandeau-home/<?php echo $home_bandeau->id_bandeau; ?>" class="orange-perso lien-icon"><i class="fa fa-pencil-square" aria-hidden="true"></i></a></td>
                            <td><a href="accueil/delete-bandeau-home/<?php echo $home_bandeau->id_bandeau; ?>" class="orange-perso lien-icon"><i class="fa fa-window-close" aria-hidden="true"></i></a></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
            <!-- Formulaire d'ajout -->
            <h4>&bull; <u>Ajouter un bandeau</u></h4>
            <form action="./accueil/add-bandeau-home" method="post" class="text-white">
                <div class="form-group">
                    <label for="titre_1">Titre 1:</label>
                    <input type="text" name="titre_1" id="titre_1" class="form-control" placeholder="Votre titre">
                </div>
                <div class="form-group">
                    <label for="titre_2">titre 2:</label>
                    <input type="text" name="titre_2" id="titre_2" class="form-control" placeholder="Votre titre">
                </div>
                <div class="form-group">
                    <label for="titre_3">titre 3:</label>
                    <input type="text" name="titre_3" id="titre_3" class="form-control" placeholder="Votre titre">
                </div>
                <div class="form-group">
                    <label for="titre_4">titre 4:</label>
                    <input type="text" name="titre_4" id="titre_4" class="form-control" placeholder="Votre titre">
                </div>
                <div class="form-group">
                    <label for="texte_bandeau">Contenu du texte:</label>
                    <textarea name="texte_bandeau" id="texte_bandeau" rows="5" class="form-control" placeholder="Votre texte"></textarea>
                </div>
                <div class="text-right">
                    <button type="submit" class="btn bg-orange text-white">Ajouter un bandeau</button>
                </div>
            </form>
        </div>
        <hr class="hr-conseil">
        <!-- Administrer le bloc présentation -->
        <div class="p-4 my-3 home-presentation shadow">
            <h3>Administrer le bloc présentation de la page Accueil</h3>
            <!-- Liste des présentations -->
            <h4>&bull; <u>Liste des présentations</u></h4>
            <table class="table table-borderless text-white">
                <thead class="bg-orange text-white text-center">
                    <tr>
                        <th scope="col">Image</th>
                        <th scope="col">Texte</th>
                        <th scope="col">Modification</th>
                        <th scope="col">Suppression</th>
                    </tr>
                </thead>
                <tbody class="text-center">
                    <?php foreach ($home_presentations as $home_presentation) { ?>
                        <tr>
                            <td><img src="<?php echo BASE_FOLDER; ?>/assets/images/<?php echo $home_presentation->image_presentation; ?>" alt="jonathan-borreil" class="img-conseil"></td>
                            <td><?php echo tailleAffichage($home_presentation->texte_presentation); ?></td>
                            <td><a href="accueil/edit-presentation/<?php echo $home_presentation->id_presentation; ?>" class="orange-perso lien-icon"><i class="fa fa-pencil-square" aria-hidden="true"></i></a></td>
                            <td><a href="accueil/delete-presentation/<?php echo $home_presentation->id_presentation; ?>" class="orange-perso lien-icon"><i class="fa fa-window-close" aria-hidden="true"></i></a></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
            <!-- Formulaire d'ajout -->
            <h4>&bull; <u>Ajouter une présentation</u></h4>
            <form action="./accueil/add-presentation" method="post" enctype="multipart/form-data" class="text-white">
            <div class="form-group">
                    <label for="image_presentation">Image:</label>
                    <input type="file" name="image" id="image_presentation" class="form-control">
                </div>
                <div class="form-group">
                    <label for="texte_presentation">Contenu de la présentation:</label>
                    <textarea name="texte_presentation" id="texte_presentation" rows="5" class="form-control" placeholder="Votre présentation"></textarea>
                </div>
                <div class="text-right">
                    <button type="submit" class="btn bg-orange text-white">Ajouter une présentation</button>
                </div>
            </form>
        </div>
        <hr class="hr-conseil">
        <!-- Administrer le bloc contact -->
        <div class="p-4 my-3 home-contact shadow">
            <h3>Administrer le bloc contact de la page Accueil</h3>
            <!-- Liste des titres du bandeau -->
            <h4>&bull; <u>Liste du contenu du bloc contact</u></h4>
            <table class="table table-borderless text-white">
                <thead class="text-center bg-orange text-white">
                    <tr>
                        <th scope="col">Adresse</th>
                        <th scope="col">Téléphone</th>
                        <th scope="col">Email</th>
                        <th scope="col">Modification</th>
                        <th scope="col">Suppression</th>
                    </tr>
                </thead>
                <tbody class="text-center">
                    <?php foreach ($home_contacts as $home_contact) { ?>
                        <tr>
                            <td><?php echo $home_contact->adresse; ?></td>
                            <td><?php echo $home_contact->telephone; ?></td>
                            <td><?php echo $home_contact->email; ?></td>
                            <td><a href="accueil/edit-contact-home/<?php echo $home_contact->id_contact; ?>" class="orange-perso lien-icon"><i class="fa fa-pencil-square" aria-hidden="true"></i></a></td>
                            <td><a href="accueil/delete-contact-home/<?php echo $home_contact->id_contact; ?>" class="orange-perso lien-icon"><i class="fa fa-window-close" aria-hidden="true"></i></a></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
            <!-- Formulaire d'ajout -->
            <h4>&bull; <u>Ajouter les contacts du cabinet</u></h4>
            <form action="./accueil/add-contact-home" method="post" class="text-white">
                <div class="form-group">
                    <label for="adresse">Adresse:</label>
                    <input type="text" name="adresse" id="adresse" class="form-control" placeholder="Votre adresse">
                </div>
                <div class="form-group">
                    <label for="telephone">Téléphone:</label>
                    <input type="text" name="telephone" id="telephone" class="form-control" placeholder="Votre téléphone">
                </div>
                <div class="form-group">
                    <label for="email">Email:</label>
                    <input type="text" name="email" id="email" class="form-control" placeholder="Votre email">
                </div>
                <div class="text-right">
                    <button type="submit" class="btn bg-orange text-white">Ajouter</button>
                </div>
            </form>
        </div>
        <hr class="hr-conseil">
        <!-- Administrer les horaires -->
        <div class="p-4 my-3 home-horaire shadow">
            <h3>Administrer les horaires de la page Accueil</h3>
            <!-- Liste des horaires du cabinat -->
            <h4>&bull; <u>Liste des horaires du cabinet</u></h4>
            <table class="table table-borderless text-white">
                <thead class="text-center bg-orange text-white">
                    <tr>
                        <th scope="col">Jour d'ouverture</th>
                        <th scope="col">Heure d'ouverture</th>
                        <th scope="col">Heure de fermeture</th>
                        <th scope="col">Modification</th>
                        <th scope="col">Suppression</th>
                    </tr>
                </thead>
                <tbody class="text-center">
                    <?php foreach ($home_horaires as $home_horaire) { ?>
                        <tr>
                            <td><?php echo $home_horaire->jour_ouverture; ?></td>
                            <td><?php echo $home_horaire->heure_ouverture; ?></td>
                            <td><?php echo $home_horaire->heure_fermeture; ?></td>
                            <td><a href="accueil/edit-horaire-home/<?php echo $home_horaire->id_horaire; ?>" class="orange-perso lien-icon"><i class="fa fa-pencil-square" aria-hidden="true"></i></a></td>
                            <td><a href="accueil/delete-horaire-home/<?php echo $home_horaire->id_horaire; ?>" class="orange-perso lien-icon"><i class="fa fa-window-close" aria-hidden="true"></i></a></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
            <!-- Formulaire d'ajout -->
            <h4>&bull; <u>Ajouter des horaires</u></h4>
            <form action="./accueil/add-horaire-home" method="post" class="text-white">
                <div class="form-group">
                    <label for="jour_ouverture">Jour d'ouverture:</label>
                    <input type="text" name="jour_ouverture" id="jour_ouverture" class="form-control" placeholder="Ajoutez un jour d'ouverture">
                </div>
                <div class="form-group">
                    <label for="heure_ouverture">Heure d'ouverture:</label>
                    <input type="text" name="heure_ouverture" id="heure_ouverture" class="form-control" placeholder="Ajoutez une heure d'ouverture">
                </div>
                <div class="form-group">
                    <label for="heure_fermeture">Heure de fermeture:</label>
                    <input type="text" name="heure_fermeture" id="heure_fermeture" class="form-control" placeholder="Ajoutez une heure de fermeture">
                </div>
                <div class="text-right">
                    <button type="submit" class="btn bg-orange text-white">Ajouter des horaires</button>
                </div>
            </form>
        </div>
        <hr class="hr-conseil">
        <!-- Administrer les publics concernés -->
        <div class="p-4 my-3 home-public shadow">
            <h3>Administrer les publics concernés de la page Accueil</h3>
            <!-- Liste des publics -->
            <h4>&bull; <u>Liste des publics</u></h4>
            <table class="table table-borderless text-white">
                <thead class="bg-orange text-white text-center">
                    <tr>
                        <th scope="col">Public</th>
                        <th scope="col">Image</th>
                        <th scope="col">Modification</th>
                        <th scope="col">Suppression</th>
                    </tr>
                </thead>
                <tbody class="text-center">
                    <?php foreach ($home_publiks as $home_publik) { ?>
                        <tr>
                            <td><img src="<?php echo BASE_FOLDER; ?>/assets/images/<?php echo $home_publik->image_publik; ?>" alt="<?php echo $home_publik->publik; ?>" class="img-publik"></td>
                            <td><?php echo $home_publik->publik; ?></td>
                            <td><a href="accueil/edit-publik/<?php echo $home_publik->id_publik; ?>" class="orange-perso lien-icon"><i class="fa fa-pencil-square" aria-hidden="true"></i></a></td>
                            <td><a href="accueil/delete-publik/<?php echo $home_publik->id_publik; ?>" class="orange-perso lien-icon"><i class="fa fa-window-close" aria-hidden="true"></i></a></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
            <!-- Formulaire d'ajout -->
            <h4>&bull; <u>Ajouter un public</u></h4>
            <form action="./accueil/add-publik" method="post" enctype="multipart/form-data" class="text-white">
                <div class="form-group">
                    <label for="publik">Public concerné:</label>
                    <input type="text" name="publik" id="publik" class="form-control" placeholder="Entrez le public concerné">
                </div>
                <div class="form-group">
                    <label for="image_publik">Image:</label>
                    <input type="file" name="image" id="image_publik" class="form-control">
                </div>
                <div class="text-right">
                    <button type="submit" class="btn bg-orange text-white">Ajouter un public</button>
                </div>
            </form>
        </div>
        <hr class="hr-conseil">
        <!-- Administrer les pathologies concernées -->
        <div class="p-4 my-3 home-patho shadow">
            <h3>Administrer les pathologies concernées de la page Accueil</h3>
            <!-- Liste des pathologies -->
            <h4>&bull; <u>Liste des pathologies</u></h4>
            <table class="table table-borderless text-white">
                <thead class="bg-orange text-white text-center">
                    <tr>
                        <th scope="col">Pathologie</th>
                        <th scope="col">Image</th>
                        <th scope="col">Modification</th>
                        <th scope="col">Suppression</th>
                    </tr>
                </thead>
                <tbody class="text-center">
                    <?php foreach ($home_pathos as $home_patho) { ?>
                        <tr>
                            <td><img src="<?php echo BASE_FOLDER; ?>/assets/images/<?php echo $home_patho->image_patho; ?>" alt="<?php echo $home_patho->pathologie; ?>" class="img-conseil"></td>
                            <td><?php echo $home_patho->pathologie; ?></td>
                            <td><a href="accueil/edit-patho/<?php echo $home_patho->id_patho; ?>" class="orange-perso lien-icon"><i class="fa fa-pencil-square" aria-hidden="true"></i></a></td>
                            <td><a href="accueil/delete-patho/<?php echo $home_patho->id_patho; ?>" class="orange-perso lien-icon"><i class="fa fa-window-close" aria-hidden="true"></i></a></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
            <!-- Formulaire d'ajout -->
            <h4>&bull; <u>Ajouter une pathologie</u></h4>
            <form action="./accueil/add-patho" method="post" enctype="multipart/form-data" class="text-white">
                <div class="form-group">
                    <label for="pathologie">Pathologie:</label>
                    <input type="text" name="pathologie" id="pathologie" class="form-control" placeholder="Entrez une pathologie">
                </div>
                <div class="form-group">
                    <label for="image_patho">Image:</label>
                    <input type="file" name="image" id="image_patho" class="form-control">
                </div>
                <div class="text-right">
                    <button type="submit" class="btn bg-orange text-white">Ajouter une pathologie</button>
                </div>
            </form>
        </div>
    </div>
    <!-- FOOTER -->
    <?php include('view/inc/footer-admin.inc.php'); ?>
    <!-- CDN JS -->
    <?php include('view/inc/js.inc.php'); ?>
</body>

</html>