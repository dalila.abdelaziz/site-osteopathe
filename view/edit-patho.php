<!DOCTYPE html>
<html lang="fr">

<head>
    <?php
    include('view/inc/head-admin.inc.php');
    ?>
    <title>Admin - Modifier - Pathologie Accueil</title>
</head>

<body>
    <!-- HEADER -->
    <?php include('view/inc/header-admin.inc.php'); ?>
    <!-- BANDEAU -->
    <div class="container-fluid entete">
        <div class="container text-center bandeau">
            <h2>Admin-Accueil</h2>
            <hr class="hr-bandeau">
            <p>Bienvenue sur votre backoffice.</p>
            <p>D'ici, vous pouvez gérer le contenu de votre page Accueil.</p>
        </div>
    </div>
    <!-- FORMULAIRE DE MODIFICATION -->
    <div class="container home-contenu home-patho p-3 my-3 shadow">
        <a href="<?php echo BASE_FOLDER; ?>/admin/accueil" class="btn bg-orange text-white mb-3">Retour à la page admin-accueil</a>
        <h3>Modifier une pathologie</h3>
        <form action="../update-patho" method="post" enctype="multipart/form-data" class="text-white">
            <input type="hidden" name="id" value="<?php echo $home_patho['id_patho']; ?>">
            <input type="hidden" name="img-name" value="<?php echo $home_patho['image_patho']; ?>">
            <div class="form-group">
                <label for="pathologie">Ma présentation:</label>
                <input type="text" name="pathologie" id="pathologie" class="form-control" value="<?php echo stripslashes($home_patho['pathologie']); ?>">
            </div>
            <div class="col-2 px-0">
                <img src="<?php echo BASE_FOLDER; ?>/assets/images/<?php echo $home_patho['image_patho']; ?>" alt="<?php echo stripslashes($home_patho['pathologie']); ?>" class="img-fluid">
            </div>
            <div class="form-group">
                <label for="image">Image:</label>
                <input type="file" name="image" id="image" class="form-control">
            </div>
            <div class="text-right">
                <button type="submit" class="btn bg-orange text-white">Modifier</button>
            </div>
        </form>
    </div>
    <!-- CDN JS -->
    <?php include('view/inc/js.inc.php'); ?>
</body>

</html>