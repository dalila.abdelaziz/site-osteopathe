<!DOCTYPE html>
<html lang="fr">

<head>
    <?php
    include('view/inc/head-admin.inc.php');
    ?>
    <title>Admin - Modifier - Bandeau Accueil</title>
</head>

<body>
    <!-- HEADER -->
    <?php include('view/inc/header-admin.inc.php'); ?>
    <!-- BANDEAU -->
    <div class="container-fluid entete">
        <div class="container text-center bandeau">
            <h2>Admin-Accueil</h2>
            <hr class="hr-bandeau">
            <p>Bienvenue sur votre backoffice.</p>
            <p>D'ici, vous pouvez gérer le contenu de votre page Accueil.</p>
        </div>
    </div>
        <!-- FORMULAIRE DE MODIFICATION -->
        <div class="container home-contenu home-bandeau p-3 my-3 shadow">
        <a href="<?php echo BASE_FOLDER; ?>/admin/accueil" class="btn bg-orange text-white mb-3">Retour à la page admin-accueil</a>
        <h3>Modifier un bandeau</h3>
            <form action="../update-bandeau-home" method="post" class="text-white">
            <input type="hidden" name="id" value="<?php echo $home_bandeau['id_bandeau']; ?>">
                <div class="form-group">
                    <label for="titre_1">Titre 1:</label>
                    <input type="text" name="titre_1" id="titre_1" class="form-control" value="<?php echo stripslashes($home_bandeau['titre_1']); ?>">
                </div>
                <div class="form-group">
                    <label for="titre_2">titre 2:</label>
                    <input type="text" name="titre_2" id="titre_2" class="form-control" value="<?php echo stripslashes($home_bandeau['titre_2']); ?>">
                </div>
                <div class="form-group">
                    <label for="titre_3">titre 3:</label>
                    <input type="text" name="titre_3" id="titre_3" class="form-control" value="<?php echo stripslashes($home_bandeau['titre_3']); ?>">
                </div>
                <div class="form-group">
                    <label for="titre_4">titre 4:</label>
                    <input type="text" name="titre_4" id="titre_4" class="form-control" value="<?php echo stripslashes($home_bandeau['titre_4']); ?>">
                </div>
                <div class="form-group">
                    <label for="texte_bandeau">Contenu du texte:</label>
                    <textarea name="texte_bandeau" id="texte_bandeau" rows="5" class="form-control"><?php echo stripslashes($home_bandeau['texte_bandeau']); ?></textarea>
                </div>
                <div class="text-right">
                    <button type="submit" class="btn bg-orange text-white">Modifier</button>
                </div>
            </form>
        </div>
    <!-- CDN JS -->
    <?php include('view/inc/js.inc.php'); ?>
</body>

</html>