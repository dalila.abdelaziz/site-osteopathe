<!DOCTYPE html>
<html lang="fr">

<head>
    <?php
    include('view/inc/head.inc.php');
    ?>
    <title>Conseils</title>
</head>

<body>
    <!-- HEADER -->
    <?php include('view/inc/header.inc.php'); ?>
    <!-- Gérer la taille du texte affichée -->
    <?php
    // Réduire la taille des texte à 50 caractères
    function tailleAffichage($texte)
    {
        if (strlen($texte) >= 100) {
            $texte  = trim(substr($texte, 0, 100));
            $texte .= '...';
        }
        return $texte;
    }
    ?>
    <!-- BANDEAU -->
    <div class="container-fluid entete">
        <div class="container text-center bandeau">
            <?php
            foreach ($conseilBandeaux as $conseilBandeau) { ?>
                <h2><?php echo stripslashes($conseilBandeau->titre); ?></h2>
                <hr class="hr-bandeau">
                <p><?php echo stripslashes($conseilBandeau->soustitre); ?></p>
                <p><?php echo stripslashes($conseilBandeau->paratexte); ?></p>
            <?php
            }
            ?>
        </div>
    </div>
    <!-- ARTICLES -->
    <div class="container conseil">
        <div class="row mt-5">
            <?php
            foreach ($conseilArticles as $conseilArticle) { ?>
                <div class="col-12 col-sm-6 col-lg-4 mb-3">
                    <div class="card" style="width: 18rem;">
                        <img src="<?php echo BASE_FOLDER; ?>/assets/images/<?php echo $conseilArticle->image; ?>" alt="<?php echo $conseilArticle->titre_article; ?>" class="img-fluid card-img-top">
                        <div class="card-body">
                            <h5 class="card-title"><?php echo stripslashes($conseilArticle->titre_article); ?></h5>
                            <p class="card-text"><?php echo stripslashes(tailleAffichage($conseilArticle->texte)); ?></p>
                            <a href="#" class="btn bg-orange text-white pull-right">Voir la suite</a>
                        </div>
                    </div>
                </div>
            <?php
            }
            ?>
        </div>
    </div>
    <!-- ME CONTACTER -->
    <div class="container-fluid entete">
        <div class="container text-center bandeau">
            <ul class="list-unstyled">
                <li><img src="assets/images/icone-doctolib.png" alt="doctolib" class="img-fluid doctolib"><a href="https://www.doctolib.fr/osteopathe/toulouse/jonathan-borreil">Prendre RDV</a></li>
                <li><img src="https://img.icons8.com/fluent/48/000000/google-logo.png"/><a href="https://goo.gl/maps/rS7ebvaUHyejBmhJ6">Me trouver</a></li>
            </ul>
        </div>
    </div>
    <!-- CDN JS -->
    <?php include('view/inc/js.inc.php'); ?>
    <script>
        $(window).on('load', function() {
            var h = 0;
            $('.card').each(function() {
                if ($(this).height() > h) h = $(this).height();
                // console.log("height", $(this).height());
            });
            //console.log("h=>",h);
            $('.card').height(h);
        })
    </script>
</body>

</html>