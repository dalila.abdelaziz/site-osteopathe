<!DOCTYPE html>
<html lang="fr">

<head>
    <?php include('view/inc/admin-head.inc.php'); ?>
    <title>Modifier InfoComp - Tarifs & Contacts</title>
</head>

<body>
    <?php include('view/inc/admin-header.inc.php'); ?>
    <div class="container py-3">
        <div class="p-4 my-3 shadow bg-dark">
            <h3>Modifier un groupe d'Info Complémentaires de la page Tarifs & Contacts :</h3>
            <form action="../update-infocomp" method="post">
                <input type="hidden" name="id" value="<?php echo $tarifscontactsInfoComp['id']; ?>">
                <div class="form-group">
                    <label for="info1">Information 1 :</label>
                    <input type="text" name="info1" id="info1" class="form-control" value="<?php echo $tarifscontactsInfoComp['info1']; ?>">
                </div>
                <div class="form-group">
                    <label for="info2">Information 2 :</label>
                    <input type="text" name="info2" id="info2" class="form-control" value="<?php echo $tarifscontactsInfoComp['info2']; ?>">
                </div>
                <div class="form-group">
                    <label for="info3">Information 3 :</label>
                    <input type="text" name="info3" id="info3" class="form-control" value="<?php echo $tarifscontactsInfoComp['info3']; ?>">
                </div>
                <div class="form-group">
                    <label for="info4">Information 4 :</label>
                    <input type="text" name="info4" id="info4" class="form-control" value="<?php echo $tarifscontactsInfoComp['info4']; ?>">
                </div>
                <div class="form-group">
                    <label for="info5">Information 5 :</label>
                    <input type="text" name="info5" id="info5" class="form-control" value="<?php echo $tarifscontactsInfoComp['info5']; ?>">
                </div>
                <div class="text-center">
                    <button type="submit" class="btn btn-warning">Modifier</button>
                </div>
            </form>
        </div>
    </div>
    <?php include('view/inc/admin-footer.inc.php'); ?>
    <?php include('view/inc/admin-js.inc.php'); ?>
</body>

</html>