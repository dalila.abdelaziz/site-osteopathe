<?php
// On met en place un namespace qui représente une manière abstraite de ranger les fichiers. Càd que l'on crée des dossiers virtuels auxquels sont associés les fichiers.
// Les namespaces permettent ensuite l'utilisation de use.
namespace OsteoMvc;

class Autoloader
{
    // La fonction register permet d'enregistrer notre autoloader dans le moteur PHP.
    // Cette méthode enregistre l'automatisation du lancement de l'autoload dès que l'on appelle une classe (comme lors d'une instanciation de contrôleur).
    static function register()
    {
        spl_autoload_register(array(__CLASS__, 'autoload'));
    }
    // La fonction autoload remplace le namespace de la classe chargée et le remplace par le chemin physique d'accès à la classe.
    static function autoload($className)
    { // $className peut être de la forme OsteoMvc\Controller\ClassNameController pour un contrôleur ou de la forme OsteoMvc\Model\ClassNameController pour un modèle.
        // On utilise une condition pour vérifier si l'on traite un contrôleur ou bien un modèle.
        if (strpos($className, '\Controller')) {
            // On remplace le nom du namespace par du vide pour isoler le nom du fichier de la classe à charger.
            $className = str_replace('OsteoMvc\\Controller\\', '', $className);
            // On déclenche un require de ce fichier dans la structure physique des dossiers du projet.
            require SITE_ROOT . '\/controller\/' . $className . '.php';
        } elseif (strpos($className, '\Model')) {
            $className = str_replace('OsteoMvc\\Model\\', '', $className);
            require SITE_ROOT . '\/model\/' . $className . '.php';
        }
    }
}
