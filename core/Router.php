<?php

if (!isset($_SESSION)) session_start();

use OsteoMvc\Autoloader;
use OsteoMvc\Controller\OsteopathieController;
use OsteoMvc\Controller\TarifsContactsController;

// On intègre ici le fichier de configuration (config.php).
require_once("config.php");
// On ajoute l'autoloader (son script) dans le fichier de la classe afin d'avoir des require dynamiques (rendus automatiques).
require_once("core/Autoloader.php");
// On appelle la méthode statique de la classe pour enregistrer l'autoloader sur le serveur (moteur PHP).
// Pour rappel, les méthodes statiques ne sont pas appelées à l'aide de -> mais bien de ::
Autoloader::register();

class Router
{
    // PROPRIETES
    // On déclare un tableau associatif qui sera composé de la sorte.
    // $routes[nomDeRoute] = fonction;
    private $routes;
    // On déclare une propriété pour mémoriser le Controller concerné par l'action.
    private $currentController;
    // CONSTRUCTEUR
    public function __construct()
    {
        // On intégrera ici les routes disponibles dans le projet.
        // Route 1 : Homepage
        $this->add_route("/accueil", function ($params) {
            echo "Bienvenue sur le site de Jonathan Borreil - Ostéopathe D.O.";
            switch ($params[0]) {
                    //         case 'osteopathie':
                    //             echo 'Notre page sur l\'Osteopathie';
                    //             // Comme il y a osteopathie dans la barre d'adresse, on instancie un OsteopathieController.
                    //             break;
                    // case 'tarifs-contact':
                    //     echo 'Notre page Tarifs & Contacts';
                    //     break;
                    //         case 'conseils':
                    //             echo 'Notre page Conseils';
                    //             break;
            }
            //     if (!isset($params[1])) {
            //         $params[1] = 'user';
            //     }
            //     switch ($params[1]) {
            //         case 'indications-lombalgie':
            //             echo 'Notre article à propos des lombalgies';
            //             break;
            // }
        });
        // Route 2 : Admin
        $this->add_route("/admin", function ($params) {
            // echo "Bienvenue à toi ô grand créateur";
            // print_r($params);
            // On analyse la nature des données devant être prises en charge et on en déduit le contrôleur impliqué. 
            switch ($params[0]) {
                case 'tarifs-contacts':
                    // Puisqu'il y a tarifs-contacts dans la barre d'adresse (url), on instancie un TarifsContactsController. 
                    $this->currentController = new TarifsContactsController();
                    break;
                case 'osteopathie':
                    // On instancie ici un OsteopathieController puisqu'on aura osteopathie dans l'url.
                    $this->currentController = new OsteopathieController();
                    break;
            }
            if (!isset($params[1])) {
                $params[1] = 'list';
                // if($params[0] = ['tarifs-contacts']){
                //     $params[1] = 'list-tc';
                // }elseif($params[0]){
                //     $params[1] = 'list-o';
                // }
            }
            // Puis on analyse quelle action effectuer selon la route empruntée.
            switch ($params[1]) {
                    // Gestion d'affichage des listes appartenant à la page tarifs-contacts. 
                case 'list':
                    $this->currentController->listAdmin();
                    break;
                    // TARIFS & CONTACTS
                    // Gestion des bandeaux
                case 'add-bandeau':
                    // Ici la route empruntée est add-bandeau, on déclenche donc la méthode addBandeau du currentController correspondant à l'envoie du formulaire (action demandée).
                    // Le routeur reçoit donc des données en $_POST (il n'y aura pas de $_FILES ici car il n'y a aucun fichier image à prendre en charge).
                    $this->currentController->addBandeau($_POST);
                    $_SESSION['message-bandeau'] = 'Bandeau ajouté avec succès !';
                    header('Location:./');
                    break;
                case 'edit-bandeau':
                    // La route d'édition ressemble donc à ça : http://localhost/projets/osteo-mvc/admin/tarifs-contacts/edit/1
                    // $params[2] équivaut donc à l'id se trouvant tout au bout du chemin d'accès (route). 
                    $this->currentController->editBandeau($params[2]);
                    break;
                case 'update-bandeau':
                    // Ici la route empruntée est update-bandeau, on déclenche donc la méthode updateBandeau du currentController correspondant à la modification du formulaire d'un bandeau (action demandée).
                    // Le routeur reçoit donc des données en $_POST (il n'y aura pas de $_FILES ici car il n'y a aucun fichier image à prendre en charge).
                    $this->currentController->updateBandeau($_POST);
                    $_SESSION['message-bandeau'] = 'Bandeau modifié avec succès !';
                    header('Location:./');
                    break;
                case 'delete-bandeau':
                    $this->currentController->deleteBandeau($params[2]);
                    $_SESSION['message-bandeau'] = 'Bandeau supprimé avec succès !';
                    header('Location:../');
                    break;
                    // Gestion des tarifs
                case 'add-tarif':
                    $this->currentController->addTarif($_POST);
                    $_SESSION['message-tarif'] = 'Tarif et/ou Prestation ajouté(e)(s) avec succès !';
                    header('Location:./');
                    break;
                case 'edit-tarif':
                    $this->currentController->editTarif($params[2]);
                    break;
                case 'update-tarif':
                    $this->currentController->updateTarif($_POST);
                    $_SESSION['message-tarif'] = 'Tarif et/ou Prestation modifié(e)(s) avec succès !';
                    header('Location:./');
                    break;
                case 'delete-tarif':
                    $this->currentController->deleteTarif($params[2]);
                    $_SESSION['message-tarif'] = 'Tarif et/ou Prestation supprimé(e)(s) avec succès !';
                    header('Location:../');
                    break;
                    // Gestion des contacts
                case 'add-contact':
                    $this->currentController->addContact($_POST);
                    $_SESSION['message-contact'] = 'Contact ajouté avec succès !';
                    header('Location:./');
                    break;
                case 'edit-contact':
                    $this->currentController->editContact($params[2]);
                    break;
                case 'update-contact':
                    $this->currentController->updateContact($_POST);
                    $_SESSION['message-contact'] = 'Contact modifié avec succès !';
                    header('Location:./');
                    break;
                case 'delete-contact':
                    $this->currentController->deleteContact($params[2]);
                    $_SESSION['message-contact'] = 'Contact supprimé avec succès !';
                    header('Location:../');
                    break;
                    // Gestion des infos complémentaires
                case 'add-infocomp':
                    $this->currentController->addInfoComp($_POST);
                    $_SESSION['message-infocomp'] = 'Information complémentaire ajoutée avec succès !';
                    header('Location:./');
                    break;
                case 'edit-infocomp':
                    $this->currentController->editInfoComp($params[2]);
                    break;
                case 'update-infocomp':
                    $this->currentController->updateInfoComp($_POST);
                    $_SESSION['message-infocomp'] = 'Information complémentaire modifiée avec succès !';
                    header('Location:./');
                    break;
                case 'delete-infocomp':
                    $this->currentController->deleteInfoComp($params[2]);
                    $_SESSION['message-infocomp'] = 'Information complémentaire supprimée avec succès !';
                    header('Location:../');
                    break;
                    // OSTEOPATHIE
                case 'add-indication':
                    // La route add-indication est déclenchée suite à l'envoie d'un formulaire Indication.
                    // Le routeur reçoit donc des données en $_POST ET en $_FILES dans la mesure où il y aura des fichiers images à prendre en charge.
                    $this->currentController->addIndication($_POST, $_FILES);
                    $_SESSION['message-indication'] = 'Indication ajoutée avec succès !';
                    header('Location:' . BASE_FOLDER . '/admin/osteopathie');
                    break;
                case 'edit-indication':
                    $this->currentController->editIndication($params[2]);
                    break;
                case 'update-indication':
                    $this->currentController->updateIndication($_POST, $_FILES);
                    $_SESSION['message-indication'] = 'Indication modifiée avec succès !';
                    header('Location:' . BASE_FOLDER . '/admin/osteopathie');
                    break;
                case 'delete-indication':
                    $this->currentController->deleteIndication($params[2]);
                    $_SESSION['message-indication'] = 'Indication supprimée avec succès !';
                    header('Location:' . BASE_FOLDER . '/admin/osteopathie');
                    break;
                    // Si aucun des cas précédents n'est rencontré, on met en place une redirection par défaut vers la page d'accueil du back-office. 
                    // default:
                    //     header('Location:' . BASE_FOLDER . '/admin');
            }
        });
    }

    // METHODES
    /**
     * Fonction qui ajoute des routes dans l'application et les associe à une fonction. 
     *
     * @param [string] $route : chaîne de caractères correspondant à la route dans le projet.
     * @param callable $closure : fonction que le routeur va déclencher si la route est appelée.
     * @return void : La fonction ne renvoie rien (void).
     */
    private function add_route($route, callable $closure)
    {
        $this->routes[$route] = $closure;
    }
    /**
     * Fonction qui sera lancée depuis la page index.php
     * Elle analyse le chemin (la route) dans la barre d'adresse du navigateur (url) et déclenche la fonction associée à la route lors de l'ajout des routes dans le constructeur.
     *
     * @return void
     */
    public function execute()
    {
        // On récupère l'adresse demandée.
        $path = $_SERVER['REQUEST_URI']; // projets/osteo-mvc/admin
        // echo $path;
        // echo "<br>";
        // echo BASE_FOLDER; projets/osteo-mvc
        // On cherche la constante BASE_FOLDER et on la remplace par du vide dans le chemin récupéré dans la barre d'adresse.
        $finalPath = str_replace(BASE_FOLDER, "", $path); // /admin
        // echo $finalPath;
        // Grâce à la méthode php strrpos, on recherche dans l'adresse retenue ($finalPath) le caractère "/" pour déterminer s'il s'agit d'une route à plusieurs niveaux (exemple : admin/accueil/contacts) ou de premier niveau (exemple : /admin).
        $lastIndex = strrpos($finalPath, "/");
        // On crée un tableau qui recevra comme entrée les "arguments" de la route, càd ce qui est après le premier niveau (accueil et contacts dans le cas de notre route exemple /admin/accueil/contacts).
        $arg = array();
        // Si le lastIndex de "/" est supérieur à 0, alors on est sur une route à plusieurs niveaux.
        if ($lastIndex > 0) {
            // Alors on éclate la chaîne de $finalPath par rapport au "/" à l'aide de la méthode explode (native à php).
            $array = explode("/", $finalPath);
            // On met à jour le finalPath qui sera égal au "/" + la 1è colonne de notre tableau (ici admin) : Array ( [0] => [1] => admin [2] => accueil [3] => contacts [4] => edit [5] => mail ).
            $finalPath = "/" . $array[1];
            // print_r($array); 
            // Nous sommes ici sur une route à plusieurs niveaux, on stocke donc les appellations des niveaux (en dehors du premier) dans le tableau des arguments.
            for ($i = 2; $i < count($array); $i++) {
                // On met le mot issu de la route dans le tableau.
                array_push($arg, $array[$i]);
            }
        }
        // else { // A ajouter en cas d'utilisation de paramètres après le "/" en tant que route définie (de base) : 
        // array_push($arg, str_replace("/", "", $finalPath));
        // $finalPath = "/";
        // print_r($arg);
        // die();
        // }

        // On vérifie si le chemin $finalPath existe dans le tableau des routes prises en charge dans le MVC.
        // On utilise donc un array_key_exists("NomColonne", tableauDansLequelChercher), cette méthode boolean renvoie true si la clé existe ou false si elle n'existe pas.
        if (array_key_exists($finalPath, $this->routes)) {
            // Si la route existe, on déclenche la fonction qui lui est associée dans le tableau.
            $this->routes[$finalPath]($arg);
        } else {
            echo "ERREUR 404 !";
        }
    }

    // GETTERS / SETTERS

}

<?php

// On vérifie s'il n'y a pas de session active, si c'est le cas, on démarre une session
if (!isset($_SESSION)) session_start();

use App\Autoloader;
use App\Controller\HomeController;
use App\Controller\ConseilController;
use App\Controller\DashboardController;

// On intègre le fichier de configuration
// Quand je fais un include ou un require_once, je ne mets que ./ pour l'appel. Je n'ai pas besoin de sortir d'un dossier car c'est un include.
require_once("./config.php");
// On ajoute l'autoloader dans le fichier de la classe afin d'avoir des require dynamiques
require_once('./core/Autoloader.php');
// On appelle la méthode static de la classe pour enregistrer l'autoloader dans le moteur PHP (serveur)
// Pour rappel, les méthodes static ne sont pas appelées avec -> mais avec ::
Autoloader::register();

class Router
{
    // Propriétés 
    // On déclare un tableau associatif qui sera composé de la sorte
    // $routes[nomDeRoute] = fonction;
    private $routes;

    // On déclare une propriété pour mémoriser le Controller concerné par l'action
    private $currentController;

    // Constructeur
    public function __construct()
    {
        // Routes du front office
        $this->add_route('/', function ($params) {
            switch ($params[0]) {
                case 'accueil':
                    echo 'Vous êtes sur la page d\'accueil';
                    break;
                case 'osteopathie':
                    echo 'Vous êtes sur la page Ostéopathie';
                    break;
                case 'tarifs-contact':
                    echo 'Vous êtes sur la page Tarifs & Contact';
                    break;
                case 'conseils':
                    // echo 'Vous êtes sur la page conseils';
                    $this->currentController = new ConseilController();
                    $this->currentController->show();
                    break;
                    // Est-ce qu'il faut ajouter une route par défaut ?
            }
        });
        // Routes du back office
        $this->add_route('/admin', function ($params) {
            // On analyse la nature des données devant être prise en charge et donc on déduit le contrôleur qui va être impliqué
            switch ($params[0]) {
                case 'dashboard':
                    // echo 'Vous êtes sur l\'accueil de votre administration';
                    $this->currentController = new DashboardController();
                    break;
                case 'accueil':
                    // echo 'Vous êtes sur la page d\'administration de la page accueil';
                    $this->currentController = new HomeController();
                    break;
                case 'osteopathie':
                    echo 'Vous êtes sur la page d\'administration de la page ostéopathie';
                    break;
                case 'tarifs-contact':
                    echo 'Vous êtes sur la page d\'administration de la page tarifs & contact';
                    break;
                case 'conseils':
                    // echo 'Vous êtes sur la page d\'administration de la page conseils';
                    $this->currentController = new ConseilController();
                    break;
            }
            // On vérifie si $params[1] n'existe pas, que l'on renseigne vide ou avec l'un des termes de la liste
            if (!isset($params[1])) {
                $params[1] = 'list';
            }
            // On analyse l'action à faire :
            switch ($params[1]) {
                case 'list':
                    $this->currentController->list();
                    break;
                    // Route admin-home
                case 'add-bandeau-home':
                    // La route add est déclenchée suite à l'envoi du formulaire
                    $this->currentController->addBandeauHome($_POST);
                    break;
                case 'add-presentation':
                    // La route add est déclenchée suite à l'envoi du formulaire
                    $this->currentController->addPresentation($_POST, $_FILES);
                    break;
                case 'add-contact-home':
                    // La route add est déclenchée suite à l'envoi du formulaire
                    $this->currentController->addContactHome($_POST);
                    break;
                case 'add-horaire-home':
                    // La route add est déclenchée suite à l'envoi du formulaire
                    $this->currentController->addHoraireHome($_POST);
                    break;
                case 'add-publik':
                    // La route add est déclenchée suite à l'envoi du formulaire
                    $this->currentController->addPublik($_POST, $_FILES);
                    break;
                case 'add-patho':
                    // La route add est déclenchée suite à l'envoi du formulaire
                    $this->currentController->addPatho($_POST, $_FILES);
                    break;
                case 'edit-bandeau-home':
                    $this->currentController->editBandeauHome($params[2]);
                    break;
                case 'edit-presentation':
                    $this->currentController->editPresentation($params[2]);
                    break;
                case 'edit-contact-home':
                    $this->currentController->editContactHome($params[2]);
                    break;
                case 'edit-horaire-home':
                    $this->currentController->editHoraireHome($params[2]);
                    break;
                case 'edit-publik':
                    $this->currentController->editPublik($params[2]);
                    break;
                case 'edit-patho':
                    $this->currentController->editPatho($params[2]);
                    break;
                case 'update-bandeau-home':
                    $this->currentController->updateBandeauHome($_POST);
                    break;
                case 'update-presentation':
                    $this->currentController->updatePresentation($_POST, $_FILES);
                    break;
                case 'update-contact-home':
                    $this->currentController->updateContactHome($_POST);
                    break;
                case 'update-horaire-home':
                    $this->currentController->updateHoraireHome($_POST);
                    break;
                case 'update-publik':
                    $this->currentController->updatePublik($_POST, $_FILES);
                    break;
                case 'update-patho':
                    $this->currentController->updatePatho($_POST, $_FILES);
                    break;
                case 'delete-bandeau-home':
                    $this->currentController->deleteBandeauHome($params[2]);
                    break;
                case 'delete-presentation':
                    $this->currentController->deletePresentation($params[2]);
                    break;
                case 'delete-contact-home':
                    $this->currentController->deleteContactHome($params[2]);
                    break;
                case 'delete-horaire-home':
                    $this->currentController->deleteHoraireHome($params[2]);
                    break;
                case 'delete-publik':
                    $this->currentController->deletePublik($params[2]);
                    break;
                case 'delete-patho':
                    $this->currentController->deletePatho($params[2]);
                    break;
                    // Route admin-conseil
                case 'add-article':
                    // La route add est déclenchée suite à l'envoi du formulaire
                    $this->currentController->addArticle($_POST, $_FILES);
                    break;
                case 'add-bandeau':
                    // La route add est déclenchée suite à l'envoi du formulaire
                    $this->currentController->addBandeau($_POST);
                    break;
                case 'edit-article':
                    $this->currentController->editArticle($params[2]);
                    break;
                case 'edit-bandeau':
                    $this->currentController->editBandeau($params[2]);
                    break;
                case 'update-article':
                    $this->currentController->updateArticle($_POST, $_FILES);
                    break;
                case 'update-bandeau':
                    $this->currentController->updateBandeau($_POST);
                    break;
                case 'delete-article':
                    $this->currentController->deleteArticle($params[2]);
                    break;
                case 'delete-bandeau':
                    $this->currentController->deleteBandeau($params[2]);
                    break;
            }
        });
    }

    // Méthodes
    /**
     * Fonction qui ajoute des routes dans l'application et les associe à une fonction 
     *
     * @param [string] $route : La chaîne de caractère correspondant à la route dans le projet
     * @param callable $closure : Une fonction que le routeur va déclencher si la route est appelée
     * @return void : la fonction ne renvoie rien
     */
    private function add_route($route, callable $closure)
    {
        $this->routes[$route] = $closure;
    }

    /**
     * Fonction qui sera lancée depuis la page index.php, elle analyse le chemin (la route) dans la barre 
     * d'adresse du navigateur et déclenche la fonction associé à la route lors de l'ajout des routes dans le constructeur
     *
     * @return void
     */
    public function execute()
    {
        // On récupère l'adresse demandée
        $path = $_SERVER['REQUEST_URI'];

        // On cherche et remplace par du vide la constante BASE_FOLDER dans le chemin récupéré dans la barre d'adresse
        $finalPath = str_replace(BASE_FOLDER, "", $path);

        //Avec la méthode PHP strrpos, on recherche dans l'adresse retenue ($finalPath) le caractère slash pour déterminer si on a à faire à une route à plusieurs niveaux ou à un seul
        $lastIndex = strrpos($finalPath, '/'); //compte tous les caractères après le slash principal

        // On crée un tableau qui recevra comme une entrée les "arguments" de la route, càd qui sont après le premoer niveau
        $arg = array();

        // Si le lastIndex de / n'est pas 0, alors on est sur une route à plusieurs niveaux.
        if ($lastIndex > 0) {
            // On éclate la chaîne $finalPath par rapport au / à l'aide de la méthode native php explode
            $array = explode('/', $finalPath);

            // On met à jour le finalPath à l'aide du tableau $arg
            $finalPath = '/' . $array[1];

            // Comme on est sur une route à plusieurs niveaux, on stocke les appelations des niveaux dans le tableau des arguments. On part de 1.
            for ($i = 2; $i < count($array); $i++) {
                // On met le mot issu de la route dans le tableau
                array_push($arg, $array[$i]);
            }
        } else {
            // Sert à créer différentes routes à partir d'un slash simple et de changer le paramètre.
            array_push($arg, str_replace('/', "", $finalPath));
            // Est déclaré pour pouvoir accepter un paramètre derrière lui, je pense. 
            $finalPath = '/';
        }

        // On vérifie si le chemin ($finalPath) existe dans le tableau des routes prises en charge dans le MVC. On utilise la méthode array_key_exists('nomColonne', tableauDansLequelChercher). Cette méthode renvoie true ou false
        if (array_key_exists($finalPath, $this->routes)) {
            // si la route existe, on déclenche la fonction associée dans le tableau
            $this->routes[$finalPath]($arg);
        } else {
            echo 'Erreur 404';
        }
    }
}
